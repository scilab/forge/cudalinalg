#include <iostream>
#include <Eigen/Core>
#include <LU.h>

#include <ctime>

using namespace Eigen;
using namespace GPU;

typedef Matrix<double, Dynamic, Dynamic> MAT;
int
main()
{
  cublasInit();
  MAT mat = MAT::Random(1024, 1024);
  PartialPivLU_gpu<MAT> lu(mat);
  PartialPivLU<MAT> lu2(mat);
  cout << (lu.matrixLU() - lu2.matrixLU()).norm() << endl;
  cublasShutdown();

  return 0;

}
