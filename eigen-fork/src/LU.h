#include <Eigen/Eigen>
#include <GPUmat.h>
#include <utilities.h>

#include <iostream>
using namespace std;

using namespace Eigen;

namespace GPU
{

  template<typename _MatrixType,int NB=8>
    class PartialPivLU_gpu : public PartialPivLU<_MatrixType>
    {
    protected:
      typedef _MatrixType MatrixType;
      typedef GPUmatrix<MatrixType> GMAT;
      enum
      {
        RowsAtCompileTime = MatrixType::RowsAtCompileTime,
        ColsAtCompileTime = MatrixType::ColsAtCompileTime,
        Options = MatrixType::Options,
        MaxRowsAtCompileTime = MatrixType::MaxRowsAtCompileTime,
        MaxColsAtCompileTime = MatrixType::MaxColsAtCompileTime
      };

      typedef typename NumTraits<typename MatrixType::Scalar>::Real RealScalar;
      typedef typename ei_plain_col_type<MatrixType, int>::type
          PermutationVectorType;
      typedef PermutationMatrix<RowsAtCompileTime, MaxRowsAtCompileTime>
          PermutationType;
      typedef PartialPivLU<_MatrixType> Base;
      typedef typename MatrixType::Scalar Scalar;
      int size;

      void
      LASWP(int N, Scalar* A, int LDA, int* IPIV2, int I0)
      {
        for (int i = 0; i < NB; ++i)
          {
            int ip = IPIV2[i];
            Blas_equivalent<Scalar>::gswap(N, A + i + I0, LDA, A + ip, LDA);
          }
        return;
      }

    public:

      PartialPivLU_gpu<MatrixType>&
      compute(const MatrixType& mat)
      {
        TIMER_INIT()
        GMAT gmat(Base::m_lu);
        TIMER_BEACON()
        VectorXi row_transpositions(NB);

        int nbtransp;
        int rows = size, cols = size;
        int jb, j;

        for (j = 1; j < min(rows, cols); j += NB)
          {
            jb = NB;
            gmat.flush_to_cpu(j, j, rows, j + jb - 1);

            Block<MatrixType> tmp(Base::m_lu.block(j - 1, j - 1, rows - j + 1,
                jb));
            ei_partial_lu_inplace(tmp, row_transpositions, nbtransp);
            gmat.flush_to_gpu(j, j, rows, j + jb - 1);

            for (int i = 0; i < NB; ++i)
              {
                Block<MatrixType> tmp(Base::m_lu.block(0, 0, rows, j - 1));
                int piv = (row_transpositions[i] += j - 1);
                tmp.row(i + j - 1).swap(tmp.row(piv));
                Base::m_rowsTranspositions[j + i - 1] = piv;
              }

            if (j + jb <= cols)
              {

                LASWP(cols - j - jb + 1, gmat.at(1, j + jb), gmat.ld(),
                    row_transpositions.data(), j - 1);

                Blas_equivalent<Scalar>::gtrsm('L', 'L', 'N', 'U', jb, cols - j
                    - jb + 1, 1.0f, gmat.at(j, j), gmat.ld(),
                    gmat.at(j, j + jb), gmat.ld());

                Blas_equivalent<Scalar>::ggemm('N', 'N', rows - j - jb + 1,
                    cols - j - jb + 1, jb, -1.0f, gmat.at(j + jb, j),
                    gmat.ld(), gmat.at(j, j + jb), gmat.ld(), 1.0f, gmat.at(j
                        + jb, j + jb), gmat.ld());

                gmat.flush_to_cpu(j, j + jb, j + jb, cols);

              }
          }
        TIMER_BEACON()
        Base::m_isInitialized = true;

        return *this;
      }

      PartialPivLU_gpu(const MatrixType& matrix) :
        Base(matrix.rows())
      {
        size = matrix.rows();
        Base::m_lu = matrix;
        compute(matrix);
        for (int k = size - 1; k >= 0; --k)
          Base::m_p.applyTranspositionOnTheRight(k,
              Base::m_rowsTranspositions.coeff(k));
      }
    };

}
