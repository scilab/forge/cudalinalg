#include <GPUmat.h>
#include <Eigen/Eigen>

#define NB 8

namespace Eigen
{

template<typename Derived >
class HHutilities
{
public:
	typedef Matrix<typename Derived::Scalar, Derived::ColsAtCompileTime,
			Derived::ColsAtCompileTime> TriangularFactorType;
	typedef typename ei_plain_diag_type<Derived>::type CoeffVect;

	TriangularFactorType
	static BlockHouseholderTriangularFactor(const Derived& V,
			const CoeffVect& coeffs)
	{
		const int k = V.cols();
		ei_assert(V.cols()==k && V.rows()>=k && "Vector storing Matrix must have same columns than coeffs size and more rows than columns");
		TriangularFactorType ret(k, k);
		for (int i = 0; i < k; i++)
		{
			int rs = V.rows() - i;
			typename Derived::Scalar Vii = V(i, i);
			V.const_cast_derived().coeffRef(i, i) = 1;
			ret.col(i).head(i).noalias() = V.block(i, 0, rs, i).adjoint()
					* V.col(i).tail(rs);
			ret.col(i).head(i) *= -coeffs(i);
			V.const_cast_derived().coeffRef(i, i) = Vii;
			// FIXME add .noalias() once the triangular product can work inplace
			ret.col(i).head(i) = ret.block(0, 0, i, i).template triangularView<
					Upper> () * ret.col(i).head(i);
			ret(i, i) = coeffs(i);
		}

		return ret;
	}

	static
	void applyBlockHouseholderOnTheLeft(Derived& A, const Derived& V,
			const CoeffVect& coeffs)
	{
		const TriangularFactorType& T = BlockHouseholderTriangularFactor(V,
				coeffs);
		const TriangularView<Derived, UnitLower>& Vtri(V);

		// A -= V T V^* A
		Matrix<typename Derived::Scalar, Derived::RowsAtCompileTime,
				Derived::ColsAtCompileTime> tmp = Vtri.adjoint() * A;
		// FIXME add .noalias() once the triangular product can work inplace
		tmp = T.template triangularView<Upper> ().adjoint() * tmp;
		A.noalias() -= Vtri * tmp;
	}

	static
	void
	applyBlockHouseholderOnTheRight(Derived& A, const Derived& V,
			const CoeffVect& coeffs)
	{
		const TriangularFactorType T = BlockHouseholderTriangularFactor(V,
				coeffs);
		const TriangularView<Derived, UnitLower> Vtri(V);

		// A -= A *V T V^
				Matrix<typename Derived::Scalar, Derived::RowsAtCompileTime,
				Derived::ColsAtCompileTime> tmp = A * Vtri;
				// FIXME add .noalias() once the triangular product can work inplace
				tmp *= T.template triangularView<Upper> ().adjoint();
				A.noalias() -= tmp * Vtri.adjoint();
			}
		};

		template<typename MatrixType,int BLOCKSIZE>
		class HouseholderBlockedQR : public HouseholderQR<MatrixType>
		{
		public:
			typedef HouseholderQR<MatrixType> Base;

			typedef Block<MatrixType, Dynamic, Dynamic> BlockType;
			typedef typename MatrixType::RealScalar RealScalar;


			void
			unblockedCompute(Block<MatrixType>& m_qr)
			{
				int rows = m_qr.rows();
				int cols = m_qr.cols();
				int size = std::min(rows, cols);

				Base::m_temp.resize(cols);

				for (int k = 0; k < size; ++k)
				{
					int remainingRows = rows - k;
					int remainingCols = cols - k - 1;

					RealScalar beta;
					m_qr.col(k).tail(remainingRows).makeHouseholderInPlace(
							Base::m_temp.coeffRef(k), beta);
					m_qr.coeffRef(k, k) = beta;

					// apply H to remaining part of m_qr from the left
					m_qr.bottomRightCorner(remainingRows, remainingCols) .applyHouseholderOnTheLeft(
							m_qr.col(k).tail(remainingRows - 1), Base::m_temp.coeffRef(k),
							&(Base::m_temp.coeffRef(k + 1)));
				}
			}

			HouseholderBlockedQR<MatrixType,BLOCKSIZE>&
			compute(const MatrixType& matrix)
			{
				int rows = matrix.rows();
				int cols = matrix.cols();
				int size = std::min(rows, cols);
				int k;

				Base::m_qr = matrix;

				Base::m_temp.resize(BLOCKSIZE);
				for (k = 0; k < size-BLOCKSIZE; k += BLOCKSIZE)
				{
					int remainingRows = rows - k;
					int remainingCols = cols - k;

					BlockType tmp=Base::m_qr.block(k,k,rows-k,BLOCKSIZE);
					unblockedCompute(tmp);

					BlockType dtmp2(Base::m_qr.block(k,k+BLOCKSIZE,rows-k,cols-k-BLOCKSIZE));

					HHutilities<BlockType>::applyBlockHouseholderOnTheLeft(dtmp2,tmp,Base::m_temp.adjoint());

				}
				BlockType tmp=Base::m_qr.bottomRightCorner(rows-k,cols-k);
				unblockedCompute(tmp);
				Base::m_isInitialized = true;
				return *this;
			}

		};

	}
