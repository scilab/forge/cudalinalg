#pragma once
#include <cublas.h>
#include <Error.h>
#include <ctime>
#include <iostream>

#define TIMER_INIT() \
  clock_t t_current; \
  clock_t t_prev=clock();


#define TIMER_BEACON() \
    t_current=clock();\
    std::cout<<static_cast<double>(t_current-t_prev)/static_cast<double>(CLOCKS_PER_SEC)<<endl;\
    t_prev=t_current;


namespace GPU
{
  template<typename Scalar>
    class Blas_equivalent
    {
    public:
      static void
      gcopy(int, Scalar*, int, Scalar*, int);
      static void
      gswap(int, Scalar*, int, Scalar*, int);
      static void
      gtrsm(char, char, char, char, int, int, Scalar, Scalar*, int, Scalar*,
          int);
      static void
      ggemm(char, char, int, int, int, Scalar, Scalar*, int, Scalar*, int,
          Scalar, Scalar*, int);
    };

  template<>
    void
    Blas_equivalent<float>::gcopy(int i1, float* i2, int i3, float* i4, int i5)
    {
      cublasScopy(i1, i2, i3, i4, i5);
      error_msg(cublasGetError());
    }

  template<>
    void
    Blas_equivalent<double>::gcopy(int i1, double* i2, int i3, double* i4,
        int i5)
    {
      cublasDcopy(i1, i2, i3, i4, i5);
      error_msg(cublasGetError());
    }

  template<>
    void
    Blas_equivalent<float>::gswap(int i1, float* i2, int i3, float* i4, int i5)
    {
      cublasSswap(i1, i2, i3, i4, i5);
      error_msg(cublasGetError());
    }

  template<>
    void
    Blas_equivalent<double>::gswap(int i1, double* i2, int i3, double* i4,
        int i5)
    {
      cublasDswap(i1, i2, i3, i4, i5);
      error_msg(cublasGetError());
    }

  template<>
    void
    Blas_equivalent<float>::gtrsm(char i1, char i2, char i3, char i4, int i5,
        int i6, float i7, float* i8, int i9, float* i10, int i11)
    {
      cublasStrsm(i1, i2, i3, i4, i5, i6, i7, i8, i9, i10, i11);
      error_msg(cublasGetError());
    }

  template<>
    void
    Blas_equivalent<double>::gtrsm(char i1, char i2, char i3, char i4, int i5,
        int i6, double i7, double* i8, int i9, double* i10, int i11)
    {
      cublasDtrsm(i1, i2, i3, i4, i5, i6, i7, i8, i9, i10, i11);
      error_msg(cublasGetError());
    }

  template<>
    void
    Blas_equivalent<float>::ggemm(char i1, char i2, int i3, int i4, int i5,
        float i6, float* i7, int i8, float* i9, int i10, float i11, float* i12,
        int i13)
    {
      cublasSgemm(i1, i2, i3, i4, i5, i6, i7, i8, i9, i10, i11, i12, i13);
      error_msg(cublasGetError());
    }

  template<>
    void
    Blas_equivalent<double>::ggemm(char i1, char i2, int i3, int i4, int i5,
        double i6, double* i7, int i8, double* i9, int i10, double i11,
        double* i12, int i13)
    {
      cublasDgemm(i1, i2, i3, i4, i5, i6, i7, i8, i9, i10, i11, i12, i13);
      error_msg(cublasGetError());
    }

}

