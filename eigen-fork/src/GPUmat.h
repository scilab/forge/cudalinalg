#pragma once

#include<Eigen/Core>

#include <Error.h>
#include <utilities.h>

using namespace std;
using namespace Eigen;

namespace GPU
{

  template<typename _MatrixType>
    class GPUmatrix
    {
    public:
      typedef _MatrixType MatrixType;
      typedef typename MatrixType::Scalar Scalar;
      typedef typename MatrixType::Scalar* GPTR;
      enum
      {
        RowsAtCompileTime = MatrixType::RowsAtCompileTime,
        ColsAtCompileTime = MatrixType::ColsAtCompileTime,
        Options = MatrixType::Options,
        MaxRowsAtCompileTime = MatrixType::MaxRowsAtCompileTime,
        MaxColsAtCompileTime = MatrixType::MaxColsAtCompileTime
      };
      GPTR gpuptr;
    protected:
      int _ld;
      MatrixType& ref;

    public:

      int
      ld()
      {
        return _ld;
      }
      ;

      GPUmatrix(const MatrixType& other) :
        ref(const_cast<MatrixType&> (other))
      {
        int r = ref.rows();
        int c = ref.cols();
        _ld = r;
        error_msg(cublasAlloc(r * c, sizeof(Scalar),
            reinterpret_cast<void**> (&gpuptr)));
        error_msg(cublasSetMatrix(r, c, sizeof(Scalar), ref.data(), _ld,
            gpuptr, _ld));
      }

      GPUmatrix(const GPUmatrix<MatrixType>& other) :
        ref(other)
      {
        int r = other.rows();
        int c = other.cols();
        _ld = r;
        error_msg(cublasAlloc(r * c, sizeof(Scalar),
            reinterpret_cast<void**> (&gpuptr)));
        Blas_equivalent<Scalar>::gcopy(r * c, other.gpuptr, 1, gpuptr, 1);
        error_msg(cublasGetError());
      }

      ~GPUmatrix()
      {
        error_msg(cublasFree(gpuptr));
      }

      inline GPTR
      at(int i, int j)
      {
        return gpuptr + (i - 1) + (j - 1) * _ld;
      }

      void
      flush_to_cpu(int i0 = 1, int j0 = 1, int i = -1, int j = -1)
      {
        int i1 = (i < 0) ? ref.rows() : i;
        int j1 = (i < 0) ? ref.cols() : j;
        Scalar* ptr = &(ref.data()[(i0 - 1) + (j0 - 1) * _ld]);
        error_msg(cublasGetMatrix(i1 - i0 + 1, j1 - j0 + 1, sizeof(Scalar), at(
            i0, j0), _ld, ptr, _ld));
      }

      void
      flush_to_gpu(int i0 = 1, int j0 = 1, int i = -1, int j = -1)
      {
        int i1 = (i < 0) ? ref.rows() : i;
        int j1 = (i < 0) ? ref.cols() : j;
        Scalar* ptr = &(ref.data()[(i0 - 1) + (j0 - 1) * _ld]);
        error_msg(cublasSetMatrix(i1 - i0 + 1, j1 - j0 + 1, sizeof(Scalar),
            ptr, _ld, at(i0, j0), _ld));

      }

    };

}

