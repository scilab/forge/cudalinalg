#pragma once

#include <cublas.h>
#include <string>
#include <stdexcept>

using namespace std;

template<typename status>
  inline void
  error_msg(status);

template<>
  inline void
  error_msg<cublasStatus> (cublasStatus status)
  {
    ei_assert(status != CUBLAS_STATUS_NOT_INITIALIZED
        && "Cublas Not Initialized !");
    ei_assert(status != CUBLAS_STATUS_ALLOC_FAILED
        && "Cublas Allocation failed !");
    ei_assert(status != CUBLAS_STATUS_INVALID_VALUE
        && "Cublas not valid value !");
    ei_assert(status != CUBLAS_STATUS_ARCH_MISMATCH && "Cublas arch mismatch !");
    ei_assert(status != CUBLAS_STATUS_MAPPING_ERROR && "Cublas mapping error !");
    ei_assert(status != CUBLAS_STATUS_EXECUTION_FAILED
        && "Cublas execution failed !");
    ei_assert(status != CUBLAS_STATUS_INTERNAL_ERROR
        && "Cublas internal error !");
  }
