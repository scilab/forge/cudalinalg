%module gfft


%include "matrix.i"

%apply (double* matrixAsInput,int rows,int cols){(double *inputreal, int m1, int n1),(double* inputcomplex,int m2,int n2)}
%apply (double** matrixAsArgOutput,int* rows,int* cols){(double **outputreal,int* m3,int* n3),(double** outputcomplex,int* m4,int* n4)}


%inline %{

extern void computefft(double* ,double* ,
		double* ,double* ,
		int ,int );

void gpu_fft(double* inputreal,int m1,int n1,
		double* inputcomplex,int m2,int n2,
		double** outputreal,int* m3,int* n3,
		double** outputcomplex,int* m4,int* n4)
		{
			*m4=*m3=m1;
			*n4=*n3=n1;
			*outputreal=(double*)malloc(m1*n1*sizeof(double));
			*outputcomplex=(double*)malloc(m1*n1*sizeof(double));
			computefft(inputreal,inputcomplex,*outputreal,*outputcomplex,n1,m1);
		}
		
extern void computeifft(double* ,double* ,
		double* ,double* ,
		int ,int );
		
void gpu_ifft(double* inputreal,int m1,int n1,
		double* inputcomplex,int m2,int n2,
		double** outputreal,int* m3,int* n3,
		double** outputcomplex,int* m4,int* n4)
		{
			*m4=*m3=m1;
			*n4=*n3=n1;
			*outputreal=(double*)malloc(m1*n1*sizeof(double));
			*outputcomplex=(double*)malloc(m1*n1*sizeof(double));
			computeifft(inputreal,inputcomplex,*outputreal,*outputcomplex,n1,m1);
		}
%}
