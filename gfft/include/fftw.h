#include <cufft.h>
#include <cuda.h>
#include <cublas.h>
#include <cstdlib>

void computefft(
		double* inputreal,double* inputcomplex,
		double* outputreal,double* outputcomplex,
		int rows,int cols);

void computeifft(
		double* inputreal,double* inputcomplex,
		double* outputreal,double* outputcomplex,
		int rows,int cols);
