cmake_minimum_required(VERSION 2.8)

PROJECT(gfft)


find_package(CUDA REQUIRED)

include_directories(include)
include_directories(${CUDA_INCLUDE_DIRS})

FILE(GLOB
    SOURCES_CPP
    src/*
)

add_library(gfft SHARED
${SOURCES_CPP}
)

target_link_libraries(gfft
${CUDA_CUFFT_LIBRARIES} ${CUDA_CUBLAS_LIBRARIES}
)
