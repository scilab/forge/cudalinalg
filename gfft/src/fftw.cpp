
extern "C"
{
#include <fftw.h>
}
#include <exception>
#include <iostream>
#include <cuda_runtime.h>
#include <cuda/cufft.h>

using namespace std;

#define SAFE(command) \
{ \
  cublasStatus res=command; \
  if(res) \
  { \
  cout<<res; \
     \
    } \
}

void do_cufft_2d(cufftDoubleComplex* input,cufftDoubleComplex* output,int rows,int cols)
{
  cufftHandle plan;
  cufftPlan2d(&plan,rows,cols,CUFFT_Z2Z);
  cufftExecZ2Z(plan,input,output,CUFFT_FORWARD);
  
  return;
}

void do_cuifft_2d(cufftDoubleComplex* input,cufftDoubleComplex* output,int rows,int cols)
{
  cufftHandle plan;
  cufftPlan2d(&plan,rows,cols,CUFFT_Z2Z);
  cufftExecZ2Z(plan,input,output,CUFFT_INVERSE);

  return;
}



cufftDoubleComplex* convert_matrix_htd(double* real_part,double* imaginary_part,int length)
{
    cufftDoubleComplex* retvalue;
    cublasAlloc(length,sizeof(cufftDoubleComplex),(void**)(&retvalue));
    double* gptr=reinterpret_cast<double*>(retvalue);
    SAFE( cublasSetVector(length,sizeof(double),real_part,1,gptr,2) )
    SAFE( cublasSetVector(length,sizeof(double),imaginary_part,1,gptr+1,2) )
    return retvalue;
}

void convert_matrix_dth(cufftDoubleComplex* input,double* real_part,double* imaginary_part,int length)
{
    double* gptr=reinterpret_cast<double*>(input);
    cublasGetVector(length,sizeof(double),gptr,2,real_part,1);
    cublasGetVector(length,sizeof(double),gptr+1,2,imaginary_part,1);
    cublasFree(input);
    return;
}



void computefft(double* inputreal,double* inputcomplex,
		double* outputreal,double* outputcomplex,
		int rows,int cols)
{
	cublasInit();
	cufftDoubleComplex* cudafftoutput;
	cufftDoubleComplex* cudafftinput=convert_matrix_htd(inputreal,inputcomplex,rows*cols);
    cublasAlloc(rows*cols,sizeof(cufftDoubleComplex),(void**)(&cudafftoutput));
    do_cufft_2d(cudafftinput,cudafftoutput,rows,cols);
    convert_matrix_dth(cudafftoutput,outputreal,outputcomplex,rows*cols);
    return;
}

void computeifft(double* inputreal,double* inputcomplex,
		double* outputreal,double* outputcomplex,
		int rows,int cols)
{
	cublasInit();
	cufftDoubleComplex* cudafftoutput;
	cufftDoubleComplex* cudafftinput=convert_matrix_htd(inputreal,inputcomplex,rows*cols);
    cublasAlloc(rows*cols,sizeof(cufftDoubleComplex),(void**)(&cudafftoutput));
    do_cuifft_2d(cudafftinput,cudafftoutput,rows,cols);
    convert_matrix_dth(cudafftoutput,outputreal,outputcomplex,rows*cols);
    return;
}
