#include <fftw.h>
#include <iostream>


double* gen(int length)
{
  double* ret=new double[length];
  for(int i=0;i<length;++i)
    ret[i]=static_cast<double>(rand())/RAND_MAX;
  return ret;
}

void display(double* input, int length)
{
  for(int i=0;i<length;++i)
    cout<<input[i]<<" ";
  cout<<endl;
}


int main()
{
  double* sample0=gen(8);
  double* sample1=gen(8);
  double* sample2=new double[16];
  display(sample0,8);
  display(sample1,8);

  cufftDoubleComplex* ptr=convert_matrix(sample0,sample1,8);
  cublasGetVector(8,sizeof(cufftDoubleComplex),ptr,1,sample2,1);
  display(sample2,16);
  pair<double*,double*> ret=get_and_destroy(ptr,8);
  display(ret.first,8);
  
}