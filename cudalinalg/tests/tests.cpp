#include <lapack.hxx>
#include <cassert>
#include <cstdlib>
#include <string>
#include <iostream>
#include <vector>
#include <cstdlib>
using namespace std;

template<typename T>
void display_matrix ( int M,int N,T* mat )
{

    cout << " ";

    for ( int i=0;i<M;i++ )
    {
        for ( int j=0;j<N;j++ )
        {
            cout << mat[i+M*j] << " ";
        }
        cout << ";"<<endl;
    }
}

template<typename T>
T* gen_rand_matrix ( int M,int N )
{
    T* tmp=new T[M*N];
    for ( int i=0;i<M;i++ )
    {
        for ( int j=0;j<N;j++ )
        {
            T nb = ( rand() /static_cast<T> ( RAND_MAX ) );
            tmp[i+j*M]= nb;
        }
    }
    return tmp;
}

template<typename T>
T* diff_matrix ( int MA,int NA,T* A,int MB,int NB,T* B )
{
    assert ( NA==NB && MA == MB );
    T* result=new T[MA*NA];
    for ( int i=0;i<MA;++i )
    {
        for ( int j=0;j<NA;++j )
        {
            double tmp = ( A[i+MA*j]-B[i+MB*j] );
            result[i+MA*j]=tmp;
        }
    }
    return result;
}

template<typename T>
T norm(T* mat,int N,int M)
{
    T res=0;
    for (int i=0;i<M*N;i++)
        res+=mat[i]*mat[i];
    return res;
}

void tst_sgetrf()
{
    float* A=gen_rand_matrix<float> ( 128,128 );
    pair<float*,int*> res = LUFact_float ( 128,128,A );
    pair<float*,int*> res2= LUFact_float_d ( 128,128,A );
    float* dif=diff_matrix<float> ( 128,128,res.first,128,128,res2.first );
    display_matrix<float> ( 128,128,dif );
    float norme=norm<float>(dif,128,128);
    delete [] res.first;
    delete [] res.second;
    delete [] res2.first;
    delete [] res2.second;
    delete [] A;
    delete [] dif;
    cout<<norme<<endl;
    assert(norme<0.0001);
}

void tst_sgeqrf()
{
    float* A=gen_rand_matrix<float> ( 128,128 );
    pair<float*,float*> res = QRFact_float ( 128,128,A );
    pair<float*,float*> res2= QRFact_float_d ( 128,128,A );
    float* dif=diff_matrix<float> ( 128,128,res.first,128,128,res2.first );
    display_matrix<float> ( 128,128,dif );
    float norme=norm<float>(dif,128,128);
    delete [] res.first;
    delete [] res.second;
    delete [] res2.first;
    delete [] res2.second;
    delete [] A;
    delete [] dif;
    cout<<norme<<endl;
    assert(norme<0.0001);
}

int main ( int argc, char *argv[] )
{
    assert ( argc == 2 );
    char* tn=argv[1];
    string tst_name ( tn );
    if ( tst_name=="sgetrf" )
        tst_sgetrf();
    if (tst_name=="sgeqrf")
        tst_sgeqrf();

    return 0;
}
