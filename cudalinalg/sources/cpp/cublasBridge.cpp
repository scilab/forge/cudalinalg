/*
 * cublasBridge.cpp
 *
 *  Created on: 17 août 2009
 *      Author: vlj
 */

#include <cublasBridge.h>
#include <iostream>
#include <cassert>

/*===================================LAPACK WRAPPER====================*/

extern "C" void sgetrf_ ( int*,int*,float*,int*,int*,int* );
extern "C" void dgetrf_ ( int*,int*,double*,int*,int*,int* );

void _getrf ( int i1,int i2, double* i3, int i4, int *i5, int& i6 )
{
    dgetrf_ ( &i1,&i2,i3,&i4,i5,&i6 );
}

void _getrf ( int i1,int i2, float* i3, int i4, int *i5, int& i6 )
{
    sgetrf_ ( &i1,&i2,i3,&i4,i5,&i6 );
}

extern "C" int slaswp_ ( int*,float*,int*,int*,int*,int*,int* );
extern "C" int dlaswp_ ( int*,double*,int*,int*,int*,int*,int* );


void _laswp ( int i1,float* i2,int i3,int i4,int i5,int* i6,int i7 )
{
    slaswp_ ( &i1,i2,&i3,&i4,&i5,i6,&i7 );
}

void _laswp ( int i1,double* i2,int i3,int i4,int i5,int* i6,int i7 )
{
    dlaswp_ ( &i1,i2,&i3,&i4,&i5,i6,&i7 );
}


extern "C" int dgeqrf_ ( int*,int*,double*,int*,double*,double*,int*,int* );
extern "C" int sgeqrf_ ( int*,int*,float*,int*,float*,float*,int*,int* );
extern "C" int dlarft_ ( const char*,const char*,const int*,const int*,double*,const int*,double*,double*,const int* );
extern "C" int slarft_ ( const char*,const char*,const int*,const int*,float*,const int*,float*,float*,const int* );

void _geqrf ( int M,int N,double* A,int LDA, double* TAU, double* WORK,int LWORK,int &INFO )
{
    dgeqrf_ ( &M,&N,A,&LDA,TAU,WORK,&LWORK,&INFO );
}

void _geqrf ( int M,int N,float* A,int LDA, float* TAU, float* WORK,int LWORK,int &INFO )
{
    sgeqrf_ ( &M,&N,A,&LDA,TAU,WORK,&LWORK,&INFO );
}

void _larft ( char direct,char storev,int N,int K,double* V,int LDV,double* tau,double* t,int LDT )
{
    dlarft_ ( &direct,&storev,&N,&K,V,&LDV,tau,t,&LDT );
}

void _larft ( char direct,char storev,int N,int K,float* V,int LDV,float* tau,float* t,int LDT )
{
    slarft_ ( &direct,&storev,&N,&K,V,&LDV,tau,t,&LDT );
}

extern "C" int dorgqr_ ( int*,int*,int*,double*,int*,double*,double*,int*,int* );
extern "C" int sorgqr_ ( int*,int*,int*,float*,int*,float*,float*,int*,int* );

void _orgqr ( int M,int N,int K,double* A,int LDA,double* TAU,double* WORK,int LWORK,int &INFO )
{
    dorgqr_ ( &M,&N,&K,A,&LDA,TAU,WORK,&LWORK,&INFO );
}

void _orgqr ( int M,int N,int K,float* A,int LDA,float* TAU,float* WORK,int LWORK,int &INFO )
{
    sorgqr_ ( &M,&N,&K,A,&LDA,TAU,WORK,&LWORK,&INFO );
}

extern "C" void strsm_ ( char*,char* c2,char* c3,char* c4,int* i1,int* i2,float* f1,float* f2,int* i3,float* f3,int* i5 );

void _trsm ( char c1,char c2,char c3,char c4,int i1,int i2,float f1,float* f2,int i3,float* f3,int i5 )
{
    strsm_ ( &c1,&c2,&c3,&c4,&i1,&i2,&f1,f2,&i3,f3,&i5 );
}
//void _trsm(char,char,char,char,int,int,double,double*,int,double*,int);

extern "C" void sgemm_(char*,char*,int*,int*,int*,float*,float*,int*,float*,int*,float*,float*,int*);

void _gemm(char c1,char c2,int i1,int i2,int i3,float f1,float* f2,int i4,float* f3,int i5,float f4,float* f5,int i6)
{
    sgemm_(&c1,&c2,&i1,&i2,&i3,&f1,f2,&i4,f3,&i5,&f4,f5,&i6);
}

/*=================================CUBLAS WRAPPER=========================*/

void cublas_gemm ( char transa, char transb, int m, int n, int k,
                   double alpha, const double *A, int lda,
                   const double *B, int ldb, double beta, double *C,
                   int ldc )
{
    cublasDgemm ( transa, transb, m, n, k,
                  alpha, A, lda ,
                  B, ldb,  beta, C,
                  ldc );
    assert(!cublasGetError());
}

void cublas_gemm ( char transa, char transb, int m, int n, int k,
                   float alpha, const float *A, int lda,
                   const float *B, int ldb, float beta, float *C,
                   int ldc )
{
    cublasSgemm ( transa, transb, m, n, k,
                  alpha, A, lda ,
                  B, ldb,  beta, C,
                  ldc );
    assert(!cublasGetError());
}

void cublas_trsm ( char side, char uplo, char transa,
                   char diag, int m, int n, double alpha,
                   const double *A, int lda, double *B,
                   int ldb )
{
    cublasDtrsm ( side, uplo, transa,
                  diag, m,  n,  alpha,
                  A, lda, B,
                  ldb );
    assert(!cublasGetError());
}

void cublas_trsm ( char side, char uplo, char transa,
                   char diag, int m, int n, float alpha,
                   const float *A, int lda, float *B,
                   int ldb )
{
    cublasStrsm ( side, uplo, transa,
                  diag, m,  n,  alpha,
                  A, lda, B,
                  ldb );
    assert(!cublasGetError());
}

void cublas_trmm ( char side, char uplo, char transa,
                   char diag, int m, int n, double alpha,
                   const double *A, int lda, double *B,
                   int ldb )
{
    cublasDtrmm ( side,  uplo,  transa,
                  diag,  m,  n,  alpha,
                  A,  lda, B,
                  ldb );
    assert(!cublasGetError());
}

void cublas_trmm ( char side, char uplo, char transa,
                   char diag, int m, int n, float alpha,
                   const float *A, int lda, float *B,
                   int ldb )
{
    cublasStrmm ( side,  uplo,  transa,
                  diag,  m,  n,  alpha,
                  A,  lda, B,
                  ldb );
    assert(!cublasGetError());
}

void cublas_copy ( int n, const double *x, int incx, double *y,
                   int incy )
{
    cublasDcopy ( n, x,  incx, y,
                  incy );
    assert(!cublasGetError());
}

void cublas_copy ( int n, const float *x, int incx, float *y,
                   int incy )
{
    cublasScopy ( n, x,  incx, y,
                  incy );
    assert(!cublasGetError());
}

void cublas_gemv ( char trans, int m, int n, double alpha,
                   const double *A, int lda, const double *x,
                   int incx, double beta, double *y, int incy )
{
    cublasDgemv ( trans,  m,  n,  alpha,
                  A,  lda, x,
                  incx,  beta, y,  incy );
    assert(!cublasGetError());
}

void cublas_gemv ( char trans, int m, int n, float alpha,
                   const float *A, int lda, const float *x,
                   int incx, float beta, float *y, int incy )
{
    cublasSgemv ( trans,  m,  n,  alpha,
                  A,  lda, x,
                  incx,  beta, y,  incy );
    assert(!cublasGetError());
}

void cublas_axpy ( int n, double alpha, const double *x, int incx,
                   double *y, int incy )
{
    cublasDaxpy ( n,  alpha,   x,  incx,
                  y,  incy );
    assert(!cublasGetError());
}

void cublas_axpy ( int n, float alpha, const float *x, int incx,
                   float *y, int incy )
{
    cublasSaxpy ( n,  alpha,   x,  incx,
                  y,  incy );
    assert(!cublasGetError());
}

void cublas_swap ( int n, float* x, int incx, float* y, int incy )
{
    cublasSswap ( n,x,incx,y,incy );
    assert(!cublasGetError());
}

void cublas_swap ( int n, double* x, int incx, double* y, int incy )
{
    cublasDswap ( n,x,incx,y,incy );
    assert(!cublasGetError());
}
