/*!
 * \file LUFact.cpp
 *
 *
 */

#include <lapack.hxx>
#include <templates/LUFact.h>
#include <iostream>
#include <set>
#include <cublasBridge.h>
#include <string.h>

/*! 
 * \brief factorise au format LU une matrice
 *	\f$ \left(\begin{array}{c c}1 & 2\\ 3 & 4\end{array}\right) \f$
 */
pair<float*,int*> LUFact_float(int rows,int cols,float* mat)
{
    return LUFact<float>(mat,rows,cols);
}

pair<float*,int*> LUFact_float_d(int rows,int cols,float* mat)
{
  int IINFO;
  int* IPIV = static_cast<int*>(malloc(rows*sizeof(int)));
  float* ret=static_cast<float*>(malloc(rows*cols*sizeof(float)));

  memcpy(ret,mat,rows*cols*sizeof(float));
  
  _getrf(rows,cols,ret,rows,IPIV,IINFO);
    return pair<float*,int*>(ret,IPIV);
}
