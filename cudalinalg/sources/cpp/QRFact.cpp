/*!
 * \file QRFact.cpp
 *
 *
 */

#include <lapack.hxx>
#include <templates/QRFact.h>
#include <cstdlib>
#include <iostream>
#include <set>
#include <cublasBridge.h>
#include <string.h>

/*! 
 * \brief factorise au format LU une matrice
 *	\f$ \left(\begin{array}{c c}1 & 2\\ 3 & 4\end{array}\right) \f$
 */
pair<float*,float*> QRFact_float(int rows,int cols,float* mat)
{
    return QRFact<float>(mat,rows,cols);
}

pair<float*,float*> QRFact_float_d(int rows,int cols,float* mat)
{
  int IINFO;
  int lwork=rows;
  float* work=static_cast<float*>(malloc(rows*cols*sizeof(float)));
  float* TAU = static_cast<float*>(malloc(rows*sizeof(float)));
  float* ret=static_cast<float*>(malloc(rows*cols*sizeof(float)));

  memcpy(ret,mat,rows*cols*sizeof(float));
  
  _geqrf(rows,cols,ret,rows,TAU,work,lwork,IINFO);
    return pair<float*,float*>(ret,TAU);
}
