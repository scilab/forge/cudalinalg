#define GRIDSETUP(i,j) 	dim3 threads(NB,NB);\
  dim3 grid((i)/NB+1,(j)/NB+1);

#include <templates/config.h>
#include <cublasBridge.h>


template<bool scalediag>
__global__
void makeLowerUnit(double* A,int LDA,int m,int n)
{
  unsigned int idr=threadIdx.x + blockIdx.x * blockDim.x;
  unsigned int idc=threadIdx.y + blockIdx.y * blockDim.y;

  if(idr<m && idc<n)
  {
    if(idc>idr)
      A[idr+LDA*idc]=0;
    if(idr==idc && scalediag)
      A[idr+LDA*idc]=1;
  }
}

template<bool scalediag>
__global__
void makeLowerUnit(float* A,int LDA,int m,int n)
{
  unsigned int idr=threadIdx.x + blockIdx.x * blockDim.x;
  unsigned int idc=threadIdx.y + blockIdx.y * blockDim.y;

  if(idr<m && idc<n)
  {
    if(idc>idr)
      A[idr+LDA*idc]=0;
    if(idr==idc && scalediag)
      A[idr+LDA*idc]=1;
  }
}


void cublas_makelowerunit(int m,int n,int lda,float* A)
{
	GRIDSETUP(m,n)
	makeLowerUnit<true><<<grid,threads>>>(A,lda,m,n);
}

__host__
void cublas_makelowerunit(int m,int n,int lda,double* A)
{
	GRIDSETUP(m,n)
	makeLowerUnit<true><<<grid,threads>>>(A,lda,m,n);
}