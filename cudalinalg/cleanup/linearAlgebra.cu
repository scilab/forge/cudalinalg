/*
 * linearAlgebra.cpp
 *
 *  Created on: 1 juin 2009
 *      Author: vlj
 */

#include "cuLapack.h"

#include "../../include/STATUS.h"
extern "C" {
#include "../../include/linearAlgebra.h"
}
#include <iostream>
#include <cstdlib>

#define SAFE_EXEC(str) \
	try{str}\
	catch(argumentError& e)\
	{return BAD_ARGUMENT;}\
	catch(algorithmFailure& e)\
	{return ALGORITHM_FAILURE;}


#ifdef COMPATIBILITY

STATUS gpuIntdgesv3(gMatrix a,gMatrix b,gMatrix* result)
{
	unsigned int MA = a->getRows();
	unsigned int NA = a->getColumns();
	unsigned int MB = b->getRows();
	unsigned int NRHS = b->getColumns();

	/*PRELIMINARY CHECK*/

	/* Check if a and b have the same number of rows */
	if(MA!=MB)
		return BAD_ARGUMENT;
	unsigned int M=MA;
	unsigned int N=NA;

	/* If both matrix are "empty", return the b "empty" matrix (convention) */
	if(MB==0)
	{
		*result=b;
		return SUCCESS;
	}
	/* TODO : Check if A and B contains Inf or NaN's */
	//unsigned int EPS,ANORM;/* TODO : Define EPS and ANORM */

	if(M==N)
	{
		gMatrix lu=new gpuMatrix(M,N);
		gMatrix lx=new gpuMatrix(M,NRHS);
		int INFO;
		int* IPIV=static_cast<int*>(malloc(N*sizeof(int)));
		if(IPIV==NULL)
			return MEMORY_FAILURE;

		/* Copy matrix content */
		cublasScopy(N*N,a->gpuPtr,1,lu->gpuPtr,1);
		/* Make LU decomposition of the copied matrix */

		SAFE_EXEC(GETRF<float>(N,N,lu->gpuPtr,N,IPIV,INFO);)

		cublasScopy(M*NRHS,b->gpuPtr,1,lx->gpuPtr,1);
		SAFE_EXEC(GETRS<float>('N',N,NRHS,lu->gpuPtr,M,IPIV,lx->gpuPtr,M,INFO);)
		*result=lx;
		return SUCCESS;
	}
	/* TODO : finish the function */
	return NOT_IMPLEMANTED;
}

STATUS intdgeqpf3(gMatrix a,gMatrix* q,gMatrix* r)
{
	int M=a->getRows();
	int N=a->getColumns();

	if(M==0||N==0)
	{
		*q=new gpuMatrix(0,0);
		*r=new gpuMatrix(0,0);
		return SUCCESS;
	}
	if(M<0||N<0)
	{
		return BAD_ARGUMENT;
	}

	/* Allocate every variable for the function */
	int INFO=0;
	*q=new gpuMatrix(M,M);
	*r=new gpuMatrix(M,N);
	cublasScopy(M*N,a->gpuPtr,1,(*r)->gpuPtr,1);
	/* TODO : output E (permutation matrix) */
	float* TAU=static_cast<float*>(malloc(MIN(M,N)*sizeof(float)));
	if(TAU==0)
		return MEMORY_FAILURE;
	gMatrix WORK=new gpuMatrix(N,BLOCK);
	SAFE_EXEC(GEQRF<float>(M,N,(*r)->gpuPtr,M,TAU,WORK->gpuPtr,N,INFO);)

	cublasScopy(M*N,(*r)->gpuPtr,1,(*q)->gpuPtr,1);

	transformU((*r)->gpuPtr,M,N);
	SAFE_EXEC(ORGQR<float>(M,N,MIN(M,N),(*q)->gpuPtr,M,TAU,WORK->gpuPtr,N,INFO);)

	delete WORK;
	free(TAU);
	/* TODO : finish the function */

	return SUCCESS;

}

STATUS intdgehrd(gMatrix a,gMatrix* q, gMatrix* h)
{
	int M=a->getRows();
	int N=a->getColumns();
	int INFO;
	*q=new gpuMatrix(M,M);
	*h=new gpuMatrix(M,N);
	cublasScopy(M*N,a->gpuPtr,1,(*h)->gpuPtr,1);
	float* TAU=static_cast<float*>(malloc(MIN(M,N)*sizeof(float)));
	gMatrix WORK=new gpuMatrix(N,BLOCK);

	GEHRD<float>(N,1,N,(*h)->gpuPtr,M,TAU,WORK->gpuPtr,M,INFO);
	cublasScopy(M*N,(*h)->gpuPtr,1,(*q)->gpuPtr,1);
	transformHRD((*h)->gpuPtr,M);
	transformQHR((*q)->gpuPtr,M,N);
	ORGHR<float>(N,1,N,(*q)->gpuPtr,N,TAU,WORK->gpuPtr,M,INFO);



	delete WORK;
	free(TAU);

	return SUCCESS;

}


STATUS intdgeev(gMatrix a,gMatrix* h)
{


	int M=a->getRows();
	int N=a->getColumns();
	int INFO;
	float WR[N];
	float WI[N];
	gMatrix tmp2=new gpuMatrix(M,N);
	*h=new gpuMatrix(M,M);
	cublasScopy(M*N,a->gpuPtr,1,(*h)->gpuPtr,1);
	float* TAU=static_cast<float*>(malloc(MIN(M,N)*sizeof(float)));
	gMatrix WORK=new gpuMatrix(N,BLOCK);

	GEEV<float>('V','N',N,(*h)->gpuPtr,M,WR,WI,tmp2->gpuPtr,M,NULL,1,WORK->gpuPtr,N*M,INFO);

	delete WORK;
	free(TAU);

	return SUCCESS;

}


#else


STATUS init()
{
#ifndef COMPATIBILITY
        //initLapack();
#endif
        cublasInit();
        return SUCCESS;
}

STATUS
gpuIntdgesv3(double* a,int MA, int NA,double* b,int MB,int NB,double* result)
{
  unsigned int NRHS = NB;

  /*PRELIMINARY CHECK*/

  /* Check if a and b have the same number of rows */
  if(MA!=MB)
    return BAD_ARGUMENT;
  unsigned int M=MA;
  unsigned int N=NA;

  /* If both matrix are "empty", return the b "empty" matrix (convention) */
  if(MB==0)
    {
      *result=*b;
      return SUCCESS;
    }

  /* TODO : Check if A and B contains Inf or NaN's */
  //unsigned int EPS,ANORM;/* TODO : Define EPS and ANORM */

  if(M==N)
  {
    double *g_lu,*g_lx;

    cublasAlloc(M*N,sizeof(double),(void**)&g_lu);
    cublasAlloc(M*NRHS,sizeof(double),(void**)&g_lx);
    int INFO;
    int* IPIV=static_cast<int*>(malloc(N*sizeof(int)));
    if(IPIV==NULL)
      return MEMORY_FAILURE;

    /* Copy matrix content */
    cublasSetMatrix(M,N,sizeof(double),a,M,g_lu,M);
    /* Make LU decomposition of the copied matrix */

    SAFE_EXEC(GETRF<double>(N,N,g_lu,N,IPIV,INFO);)

    cublasSetMatrix(M,NRHS,sizeof(double),b,M,g_lx,M);
    SAFE_EXEC(GETRS<double>('N',N,NRHS,g_lu,M,IPIV,g_lx,M,INFO);)
    cublasGetMatrix(M,N,sizeof(double),g_lx,M,result,M);

    cublasFree(g_lu);
    cublasFree(g_lx);
    return SUCCESS;
  }
	/* TODO : finish the function */
  return NOT_IMPLEMANTED;
}


STATUS
intdgeqpf3(double* a,unsigned int MA,unsigned int NA,double* q,double* r)
{
  int M=MA;
  int N=NA;

  if(M==0||N==0)
    return SUCCESS;
  if(M<0||N<0)
    return BAD_ARGUMENT;


  /* Allocate every variable for the function */
  int INFO=0;
  double *g_q,*g_r,*g_WORK;
  cublasAlloc(M*M,sizeof(double),(void**)&g_q);
  cublasAlloc(M*N,sizeof(double),(void**)&g_r);

  cublasSetMatrix(M,N,sizeof(double),a,M,g_r,M);
  /* TODO : output E (permutation matrix) */
  double* TAU=static_cast<double*>(malloc(MIN(M,N)*sizeof(double)));
  if(TAU==0)
    return MEMORY_FAILURE;
  cublasAlloc(M*BLOCK,sizeof(double),(void**)&g_WORK);
  SAFE_EXEC(GEQRF<double>(M,N,g_r,M,TAU,g_WORK,N,INFO);)

  cublasDcopy(M*N,g_r,1,g_q,1);

  transformU(g_r,M,N);
  SAFE_EXEC(ORGQR<double>(M,N,MIN(M,N),g_q,M,TAU,g_WORK,N,INFO);)

  cublasFree(g_q);
  cublasFree(g_r);
  cublasFree(g_WORK);
  free(TAU);
  /* TODO : finish the function */

  return SUCCESS;
}

STATUS
intdgehrd(double* a,unsigned int MA,unsigned int NA,double* q, double* h)
{
  int M=MA;
  int N=NA;
  int INFO;
  double *g_q,*g_h,*g_WORK;
  cublasAlloc(M*M,sizeof(double),(void**)&g_q);
  cublasAlloc(M*N,sizeof(double),(void**)&g_h);
  cublasAlloc(N*BLOCK,sizeof(double),(void**)&g_WORK);

  cublasSetMatrix(M,N,sizeof(double),a,M,h,M);
  double* TAU=static_cast<double*>(malloc(MIN(M,N)*sizeof(double)));

  GEHRD<double>(N,1,N,g_h,M,TAU,g_WORK,M,INFO);
  cublasDcopy(M*N,g_h,1,g_q,1);
  transformHRD(g_h,M);
  transformQHR(g_q,M,N);
  ORGHR<double>(N,1,N,g_q,N,TAU,g_WORK,M,INFO);

  cublasGetMatrix(M,M,sizeof(double),g_q,M,q,M);
  cublasGetMatrix(M,N,sizeof(double),g_h,M,h,M);

  cublasFree(g_q);
  cublasFree(g_h);
  cublasFree(g_WORK);
  free(TAU);

  return SUCCESS;
}

STATUS
intdgeev(double* a,unsigned int MA,unsigned int NA,double* h)
{
  int M=MA;
  int N=NA;
  int INFO;
  double* g_h;
  double* WR=new double(N);
  double* WI=new double(N);
  //gMatrix tmp2=new gpuMatrix(M,N);
  //*h=new gpuMatrix(M,M);
  cublasSetMatrix(M,N,sizeof(double),a,M,g_h,M);
  double* TAU=static_cast<double*>(malloc(MIN(M,N)*sizeof(double)));
  //gMatrix WORK=new gpuMatrix(N,BLOCK);

  //GEEV<double>('V','N',N,(*h)->gpuPtr,M,WR,WI,tmp2->gpuPtr,M,NULL,1,WORK->gpuPtr,N*M,INFO);

  free(TAU);

  return SUCCESS;
}


#endif






#define IDX(i,j) ((i)-1+((j)-1)*LDA)
#define A(i,j) (A+IDX(i,j))

STATUS gpuTest(double *a,double *b)
{
	//int M=a->getRows();
	//int N=a->getColumns();
	//gMatrix tmp=new gpuMatrix(M,N);
	//gMatrix tmp2=new gpuMatrix(M,N);
	//*b=new gpuMatrix(M,N);
	//double WR[N];
	//double WI[N];
	//int INFO=0;
	//int* IPIV=createPinnedArray(N);
	//gMatrix WORK=new gpuMatrix(M,N);
	//cublasDcopy(M*N,a->gpuPtr,1,(*b)->gpuPtr,1);
	//DGEEV('V','N',N,(*b)->gpuPtr,M,WR,WI,tmp2->gpuPtr,M,NULL,1,WORK->gpuPtr,N*M,INFO);
	//cublasSetMatrix(N,1,sizeof(double),WR,1,(*b)->gpuPtr,1);
	//cublasSetMatrix(N,1,sizeof(double),WI,1,(*b)->gpuPtr+N,1);

	return SUCCESS;
}

