/*
 * transform.cu
 *
 *  Created on: 9 juil. 2009
 *      Author: vlj
 */


template<const char uplo>
__global__
void transform(double* A,int LDA, int N)
{
	unsigned int idr=threadIdx.x+blockDim.x*blockIdx.x;
	unsigned int idc=threadIdx.y+blockDim.y*blockIdx.y;

	if(idc<N && idr<LDA)
	{
		if(uplo=='U')
		{
			if(idc<idr)
				A[idr+idc*LDA]=0;
		}
		else if(uplo=='L')
		{
			if(idc>idr)
				A[idr+idc*LDA]=0;
		}

	}
}

template<const char uplo>
__global__
void transform(float* A,int LDA, int N)
{
	unsigned int idr=threadIdx.x+blockDim.x*blockIdx.x;
	unsigned int idc=threadIdx.y+blockDim.y*blockIdx.y;

	if(idc<N && idr<LDA)
	{
		if(uplo=='U')
		{
			if(idc<idr)
				A[idr+idc*LDA]=0;
		}
		else if(uplo=='L')
		{
			if(idc>idr)
				A[idr+idc*LDA]=0;
		}

	}
}
