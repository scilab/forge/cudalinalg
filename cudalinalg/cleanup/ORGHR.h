/*
 * DORGHR.cpp
 *
 *  Created on: 30 juin 2009
 *      Author: vlj
 */

#include "cuLapack.h"

#define A(i,j) MIDX(A,LDA,i,j)
#define TAU(i) TAU[(i)-1]

template<typename T>
void ORGHR(int N, int ILO, int IHI,T* A, int LDA, T* TAU, T* WORK, int LWORK,int &INFO)
{
	int I,J;
	INFO=0;
	if(N<0)
		INFO=-1;
	if(ILO<1||ILO>MAX(1,N))
		INFO=-2;
	if(IHI<MIN(ILO,N)||IHI>N)
		INFO=-3;
	if(LDA<MAX(1,N))
		INFO=-5;
	if(INFO!=0)
		return;

	if(N==0)
		return;
	int NH=IHI-ILO;

	for(int J=IHI;J>=ILO+1;--J)
	{
		for(I=1;I<=J-1;++I)
		{
			HTD(A(I,J),0);
		}
		for(I=J+1;I<=IHI;++I)
		{
			T AIJm1=dblToHost(A(I,J-1),0);
			HTD(A(I,J),AIJm1);
		}
		for(I=IHI+1;I<=N;++I)
		{
			HTD(A(I,J),0);
		}
	}

	for(J=1;J<=ILO;++J)
	{
		for(I=1;I<=N;++I)
		{
			HTD(A(I,J),0);
		}
		HTD(A(J,J),1);
	}

	for(J=IHI+1;J<=N;++J)
	{
		for(I=1;I<=N;++I)
		{
			HTD(A(I,J),0);
		}
		HTD(A(J,J),1);
	}

	int IINFO;

	if(NH>0)
		ORGQR<T>(NH,NH,NH,A(ILO+1,ILO+1),LDA,&TAU(ILO),WORK,LWORK,IINFO);
}
