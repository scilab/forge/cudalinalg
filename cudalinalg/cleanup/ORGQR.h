/*
 * DORGQR.cpp
 *
 *  Created on: 30 juin 2009
 *      Author: vlj
 */

#include "cuLapack.h"

#define WORK(i) (WORK+(i)-1)
#define A(i,j) MIDX(A,LDA,i,j)
#define TAU(i) TAU[(i)-1]



template<typename T>
void ORGQR(int M,int N,int K,T* A,int LDA,T* TAU,T* WORK,int LWORK,int &INFO)
{

	int NB,NX,IINFO,I,IB,LDWORK,HEIGHT,WIDTH,MKK,NKK,KKK;
	int KK,KI;
	T* cpuBlock;
	T* cpuTriangularFactor;
	GRIDSETUP(M,N)

	NB=BLOCK;
	NX=BLOCK;
	LDWORK=N;

	cudaHostAlloc((void**)&cpuBlock,LDA*NB*sizeof(T),0);
	cudaHostAlloc((void**)&cpuTriangularFactor,LDA*NB*sizeof(T),0);


	if(M==0)
		INFO=-1;
	else if(N<0 || N>M)
		INFO=-2;
	else if(K<0 || K>N)
		INFO=-3;
	else if(LDA<MAX(1,M))
		INFO=-5;
	else if(LWORK<MAX(1,N))
		INFO=-8;

	if(INFO!=0)
		return;

	if(N<0)
		return;




	KI=((K-NX-1)/NB)*NB;
	KK=MIN(K,KI+NB);

	nullify(M,N,A,LDA,KK+1,N,KK);

	if(KK<N)
	{
		cublasGetMatrix(MKK,NKK,sizeof(T),A(KK+1,KK+1),LDA,cpuBlock,LDA);
		_orgqr(M-KK,N-KK,K-KK,cpuBlock,LDA,&TAU(KK+1),cpuTriangularFactor,LDA*NB,IINFO);
		cublasSetMatrix(MKK,NKK,sizeof(T),cpuBlock,LDA,A(KK+1,KK+1),LDA);
	}
	if(KK>0)
	{
		for(I=KI+1;I>=1;I-=NB)
		{
			IB=MIN(K-I+1,NB);
			HEIGHT=M-I+1;
			WIDTH=IB;
			if(I+IB<=N)
			{
				cublasGetMatrix(HEIGHT,WIDTH,sizeof(T),A(I,I),LDA,cpuBlock,LDA);
				_larft('F','C',HEIGHT,WIDTH,cpuBlock,LDA,&TAU(I),cpuTriangularFactor,LDA);
				cublasSetMatrix(IB,IB,sizeof(T),cpuTriangularFactor,LDA,WORK,LDWORK);
				LARFB<T>('L','N','F','C',HEIGHT,N-I-IB+1,IB,A(I,I),LDA,WORK,LDWORK,A(I,I+IB),LDA,WORK(IB+1),LDWORK);
			}
			cublasGetMatrix(HEIGHT,IB,sizeof(T),A(I,I),LDA,cpuBlock,LDA);
			_orgqr(HEIGHT,IB,IB,cpuBlock,LDA,&TAU(I),cpuTriangularFactor,LDA*NB,IINFO);
			cublasSetMatrix(HEIGHT,IB,sizeof(T),cpuBlock,LDA,A(I,I),LDA);

			nullify(M,N,A,LDA,I,I+IB-1,I-1);
		}
	}

	cudaFreeHost(cpuTriangularFactor);
	cudaFreeHost(cpuBlock);
}

