/*
 * cudaLapack.h
 *
 *  Created on: 2 juin 2009
 *      Author: vlj
 */

#ifndef CULAPACK_H_
#define CULAPACK_H_


#include "cublas.h"
#include "cublasBridge.h"


#include "cuda_runtime_api.h"
#include <cmath>
#include <cstdlib>



/*================Error Handling============*/

#include "Exceptions.h"


/*=================Miscalenous===============*/

#include "cudaLapackCU.h"


/*=======================Linear Algebra Function===========================*/

/*
 * DLASWP performs a serie of row interchange. Pivot for the row interchange
 * are contained in IPIV between K1 and K2.
 *
 * This is an adaptation of the original Lapack DLASWP routine (seek on netlib.org)
 *
 * Parameters :
 * - (@IN) N is the number of columns of A.
 * - (@IN/OUT) A is the matrix that will be changed.
 * - (@IN) K1 is the first element of IPIV for which a row interchange will be done.
 * - (@IN) K2 is the last element of IPIV for which a row interchange will be done.
 * - (@IN) IPIV is the vector of pivot indices.
 * - (@IN) INCX is the increment between element of IPIV.
 *
 * template<typename T>
 * void LASWP(int N,T* A,int LDA,int K1,int K2,int* IPIV, int INCX);
 */
#include "LASWP.h"


//void DTRMV(char UPLO, char TRANSA, char DIAG,
//		int N, double* A, int LDA, double* X, int INCX);

/* DGERTF compute an LU factorisation of a MxN matrix, following a blocked computation paradigm.
 * The factorisation is of the form A = P * L * U
 * with P a matrix of permutation, L a lower triangular matrix (with L(i,i)=1)
 * and U is a upper triangular matrix.
 *
 * This is an adaptation of the original Lapack DGETRG routine (seek on netlib.org)
 *
 * Parameters:
 * - (@in) M and N define the size of the matrix A.
 * - (@in/out) A is the matrix to be decomposed ; the results (L and U) are also stored in-place.
 * - (@in) LDA is the leading dimension of A.
 * - (@out) Pivot is an indice array, recording rows permutation of A.
 * - (@out) INFO gives information on the result :
 * 0 if everything is ok, >0 if matrix is singular, <0 if there is an illegal value.
 *
 * template<typename T>
 * void GETRF(int M,int N,T* A,int LDA,int* IPIV,int &INFO);
 */
#include "GETRF.h"

/* DGETRS compute the solution of the linear system A * X = B or A' * X = B
 * with A provided by it's LU factorisation (output from DGETRF).
 *
 * This is an adaptation of the original Lapack DGETRS routine (seek on netlib.org)
 *
 * Parameters :
 * - (@IN) TRANS specifies the form of the linear system:
 * N for A*X=B;
 * T or C for A'*X=B
 * - (@IN) N is the order of A.
 * - (@IN) NRHS is the number of columns of B.
 * - (@IN) A is the LU form of A, put in a single matrix.
 * - (@IN) LDA is the leading dimension of A.
 * - (@IN) IPIV is one of the output of DGETRF or DGETF2, it contains records of rows permutation.
 * - (@IN/OUT) B is the right hand side of the system on input, and the solution on output.
 * - (@IN) LDB is the leading dimension of B.
 *
 * template<typename T>
 * void GETRS(char TRANS,int N, int NRHS,T* A,int LDA,int* IPIV,T* B,int LDB, int &INFO);
 */
#include "GETRS.h"

/*==============================FOR QR DECOMPOSITION========================================*/

/*
 * DLARFB applies a real block factor H (or H') to C, from the left or the right.
 *
 * This is an adaptation of the original Lapack DLARFB routine (seek on netlib.org)
 *
 * Parameters :
 * - (@IN) SIDE is L to apply from the Left, R from the right.
 * - (@IN) TRANS is N for no transpose (of H) or T for transpose.
 * - (@IN) DIRECT is F for forward applying block reflectors (ie H(1)...H(k))
 * or is B for backward (ie H(k)...H(1)).
 * - (@IN) STOREV is C for columnwise storage of the vector defining the reflector,
 * R for rowwise storage.
 * - (@IN) M is the number of rows of C.
 * - (@IN) N is the number of columns of C.
 * - (@IN) K is the order of T, the block reflector.
 * - (@IN) V is the matrix containing the vectors defining the reflector.
 * - (@IN) LDV is the leading dimension of V.
 * - (@IN) T is the triangular representation of the block reflector.
 * - (@IN) LDT is the leading dimension of T.
 * - (@IN/OUT) C is the matrix on input, and is C*H or H*C... on output.
 * - (@IN) LDC is the leading dimension of C.
 * - (@WORKSPACE) WORK.
 * - (@IN) LDWORK is the leading dimension of WORK, considered as an array.
 *
 * template<typename T>
 * void LARFB(char SIDE, char TRANS, char DIRECT, char STOREV,
 * 		int M, int N, int K, T* V, int LDV,T* gT,int LDT, T* C, int LDC,
 * 		T* WORK,int LDWORK);
 */
#include "LARFB.h"

/*
 * DLARFG generates a real elementary reflector H of order N.
 * H*(alpha x')=(beta 0..0)'
 * where
 * H=I-TAU*(1 v')'*(1 v')
 *
 * This is an adaptation of the original Lapack DLARFG routine (seek on netlib.org)
 *
 * Parameters :
 * - (@IN) N is the order of the reflector.
 * - (@IN/OUT) ALPHA is alpha on input, BETA on output.
 * - (@IN/OUT) X on entry is V on output.
 * - (@IN) INCX is the increment between element of X.
 * - (@OUT) TAU is the value of TAU.
 *
 * template<typename T>
 * void LARFG(int N,T* ALPHA, T* X, int INCX, T &TAU);
 */
#include "LARFG.h"


/*
 * DLARF applies an elementary Householder transformation to a MxN matrix C.
 * H=I-TAU*V*V'
 *
 * This is an adaptation of the original Lapack DLARF routine (seek on netlib.org)
 *
 * Parameters :
 * - (@IN) SIDE is 'L' or 'R' to form H*C or C*H.
 * - (@IN) M is the number of rows of C.
 * - (@IN) N is the number of columns of C.
 * - (@IN) V is the vectors representing H.
 * - (@IN) INCV is the increment between elements of V.
 * - (@IN) TAU is the value of TAU.
 * - (@IN/OUT) C is the matrix on which the transformation will be applied.
 * - (@IN) LDC is the leading dimension of C.
 * - (@WORK) WORK is here for compatibility reasons...must be MxN at least.
 *
 * template<typename T>
 * void LARF(char SIDE,int M,int N,T* V,int INCV,T TAU, T* C,int LDC,T* WORK);
 */
#include "LARF.h"

/*
 * DGEQRF make the QR factorization of a real matrix A:
 *
 * A=Q*R
 *
 * This is an adaptation of the original Lapack DGEQRF routine (seek on netlib.org)
 *
 * Parameters :
 * - (@IN) M is the number of rows of A.
 * - (@IN) N is the number of columns of A.
 * - (@IN/OUT) A is a matrix on input.
 * On output, the element over the diagonal are the element of R,
 *  and the element below the diagonal contain information about Q (for DORGQR routine).
 * - (@OUT) TAU contains the scalar factor of each elemental transformation (for DORGQR).
 * - (@WORKSPACE) WORK
 * - (@IN) LWORK is the size of WORK.
 * - (@OUT) INFO is 0 if the routine succeeded.
 *
 * template<typename T>
 * void GEQRF(int M,int N,T* A,int LDA, T* TAU, T* WORK,int LWORK,int &INFO);
 */
#include "GEQRF.h"

/*
 * DORGQR creates a real orthogonal Q matrix, which is the product of K elementary reflectors
 * as returned by DGEQRF below the diagonal :
 * Q=H(1)H(2)...H(k)
 *
 * This is an adaptation of the original Lapack DORGQR routine (seek on netlib.org)
 *
 * Parameters :
 * - (@IN) M is the number of rows of Q.
 * - (@IN) N is the number of columns of Q.
 * - (@IN/OUT) On input, the matrix from DGEQRF.
 * On output, the Q matrix.
 * - (@IN) The leading dimension of A.
 * - (@IN) TAU contains scalar factor of each elemental transformation.
 * - (@WORKSPACE) WORK.
 * - (@IN) LWORK is the size of WORK.
 * - (@OUT) INFO is 0 if the routine succeeded.
 *
 * template<typename T>
 * void ORGQR(int M,int N,int K,T* A,int LDA,T* TAU,T* WORK,int LWORK,int &INFO);
 */
#include "ORGQR.h"



/*==================================For Hessenberg decomposition=====================================*/

/*
 * template<typename T>
 * void modifiedLAHR2(int N,int K,int NB, T *A,T *gA, int LDA,T* TAU,T* cT,T* gT,int LDT,T* Y,T* gY,int LDY,T &EI);
 */
#include "LAHR2.h"

/*
 * DGEHRD reduce a real general matrix A to Hessenberg form,
 * containing information about the orthogonal transformation Q such as :
 *
 * A = Q * H * Q'
 *
 * corresponding to
 *
 * Parameters :
 * - (@IN) N is the order of A.
 * - (@IN) ILO
 * - (@IN) IHI ; A is assumed to be already diagonal "before" ILO and "after" IHI.
 * - (@IN/OUT) A is the matrix that will be put in Hessenberg form,
 * the information about Q are stored under the first subdiagonal of the matrix.
 * - (@IN) LDA is the leading dimension of the matrix A.
 * - (@OUT) TAU is an array containing the scalar element of elementary reflector.
 * - (@WORKSPACE) WORK is of dimension LWORK
 * - (@IN) LWORK is the size of WORK
 * - (@OUT) INFO is 0 if successfull.
 *
 * template<typename T>
 * void GEHRD(int N, int ILO, int IHI, T* A,int LDA, T* TAU, T* WORK, int LWORK, int &INFO);
 */
#include "GEHRD.h"



/* template<typename T>
* void ORGHR(int N, int ILO, int IHI,T* A, int LDA, T* TAU, T* WORK, int LWORK,int &INFO);
*/
#include "ORGHR.h"


/*=================================For spectral Decomposition=======================================*/

/* template<typename T>
 * void LAHQR(bool WANTT,bool WANTZ,int N,int ILO,int IHI,T* H,int LDH,T* WR,T* WI,
 * 		int ILOZ,int IHIZ,T* Z,int LDZ,int &INFO);
 */
#include "LAHQR.h"

/* template<typename T>
 * void LANV2(T &A, T &B, T &C, T &D,
 * 		T &RT1R, T &RT1I, T &RT2R, T &RT2I, T &CS, T &SN);
 */
#include "LANV2.h"

/* template<typename T>
 * void HSEQR(char JOB,char COMPZ,int N,int ILO,int IHI,
 *		T* H,int LDH,T* WR, T* WI,T* Z,int LDZ,T* WORK,int LWORK, int &INFO);
 */
#include "HSEQR.h"

/* template<typename T>
 * void GEEV(char JOBVL,char JOBVR,int N,T* A,int LDA,T* WR, T* WI,
 * 		T* VL, int LDVL,T* VR, int LDVR, T* WORK, int LWORK, int &INFO);
 */
#include "GEEV.h"

#endif
