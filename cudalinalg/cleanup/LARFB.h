/*
 * DLARFB.cpp
 *
 *  Created on: 5 juil. 2009
 *      Author: vlj
 */


#define C(i,j) MIDX(C,LDC,i,j)
#define WORK(i,j) MIDX(WORK,LDWORK,i,j)
#define V(i,j) MIDX(V,LDV,i,j)
#define T(i,j) MIDX(T,LDT,i,j)

#include "cudaLapackCU.h"

template<typename T>
__global__
void update(T* C,T* WORK,int LDC,int LDWORK, int MAXI,int MAXJ)
{
    unsigned int ii=threadIdx.x+blockDim.x*blockIdx.x;
    unsigned int jj=threadIdx.y+blockDim.y*blockIdx.y;

    if (ii<MAXI && jj<MAXJ)
    {
        C[jj+LDC*ii]-=WORK[ii+LDWORK*jj];
    }

}


template<typename T>
void LARFB(char SIDE, char TRANS, char DIRECT, char STOREV,
           int M, int N, int K, T* V, int LDV,T* gT,int LDT, T* C, int LDC,
           T* WORK,int LDWORK)
{
    int I,J;
    char TRANST;

    bool bTRANS=(TRANS=='N'||TRANS=='n');
    bool bSTOREVC=(STOREV=='C'||STOREV=='c');
    bool bSTOREVR=(STOREV=='R'||STOREV=='r');
    bool bDIRECT=(DIRECT=='F'||DIRECT=='f');
    bool bSIDEL=(SIDE=='L'||SIDE=='l');
    bool bSIDER=(SIDE=='R'||SIDE=='r');

    if (M<=0 || N<=0)
        return;

    if (bTRANS)
    {
        TRANST='T';
    }
    else
    {
        TRANST='N';
    }

    if (bSTOREVC)
    {
        if (bDIRECT)
        {
            if (bSIDEL)
            {
                for (J=1;J<=K;++J)
                {
                    cublas_copy(N,C(J,1),LDC,WORK(1,J),1);
                }
                cublas_trmm('R','L','N','U',N,
                            K,1,V,LDV,WORK,LDWORK);
                if (M>K)
                {
                    cublas_gemm('T','N',N,K,M-K,
                                1,C(K+1,1),LDC,V(K+1,1),LDV,
                                1,WORK,LDWORK);
                }
                cublas_trmm('R','U',TRANST,'N',N,K,
                            1,gT,LDT,WORK,LDWORK);
                if (M>K)
                {
                    cublas_gemm('N','T',M-K,N,K,
                                -1,V(K+1,1),LDV,WORK,LDWORK,1,
                                C(K+1,1),LDC);
                }
                cublas_trmm('R','L','T','U',N,K,
                            1,V,LDV,WORK,LDWORK);

                GRIDSETUP(N,K)
                update<<<grid,threads>>>(C,WORK,LDC,LDWORK,N,K);

            }
            else if (bSIDER)
            {
                for (J=1;J<=K;++J)
                    cublas_copy(M,C(1,J),1,WORK(1,J),1);
                cublas_trmm('R','L','N','U',M,K,1,V,LDV,WORK,LDWORK);
                if (N>K)
                    cublas_gemm('N','N',M,K,N-K,1,C(1,K+1),LDC,V(K+1,1),LDV,1,WORK,LDWORK);
                cublas_trmm('R','U',TRANS,'N',M,K,1,gT,LDT,WORK,LDWORK);
                if (N>K)
                    cublas_gemm('N','T',M,N-K,K,-1,WORK,LDWORK,V(K+1,1),LDV,1,C(1,K+1),LDC);
                cublas_trmm('R','L','T','U',M,K,1,V,LDV,WORK,LDWORK);
                for (J=1;J<=K;++J)
                {
                    for (I=1;I<=M;++I)
                    {
                        T tmp=DTH(C(I,J));
                        T tmp2=DTH(WORK(I,J));
                        HTD(C(I,J),tmp-tmp2);
                    }
                }

            }
        }
        else
        {
            if (bSIDEL)
            {
                for (J=1;J<=K;++J)
                    cublas_copy(N,C(M-K+J,1),LDC,WORK(1,J),1);
                cublas_trmm('R','U','N','U',N,K,1,V(M-K+1,1),LDV,WORK,LDWORK);
                if (M>K)
                    cublas_gemm('T','N',N,K,M-K,1,C,LDC,V,LDV,1,WORK,LDWORK);
                cublas_trmm('R','L',TRANST,'N',N,K,1,gT,LDT,WORK,LDWORK);
                if (M>K)
                    cublas_gemm('N','T',M-K,N,K,-1,V,LDV,WORK,LDWORK,1,C,LDC);
                cublas_trmm('R','U','T','U',N,K,1,V(M-K+1,1),LDV,WORK,LDWORK);
                for (J=1;J<=K;++J)
                {
                    for (I=1;I<=N;++I)
                    {
                        T tmp=DTH(C(M-K+J,I));
                        T tmp2=DTH(WORK(I,J));
                        HTD(C(M-K+J,I),tmp-tmp2);
                    }
                }
            }
            else if (bSIDER)
            {
                for (J=1;J<=K;++J)
                    cublas_copy(M,C(1,N-K+J),1,WORK(1,J),1);
                cublas_trmm('R','U','N','U',M,K,1,V(N-K+1,1),LDV,WORK,LDWORK);
                if (N>K)
                    cublas_gemm('N','N',M,K,N-K,1,C,LDC,V,LDV,1,WORK,LDWORK);
                cublas_trmm('R','L',TRANS,'N',M,K,1,gT,LDT,WORK,LDWORK);
                if (N>K)
                    cublas_gemm('N','T',M,N-K,K,-1,WORK,LDWORK,V,LDV,1,C,LDC);
                cublas_trmm('R','U','T','U',M,K,1,V(N-K+1,1),LDV,WORK,LDWORK);
                if (M>K)
                    cublas_gemm('T','T',N,K,M-K,1,C(K+1,1),LDC,V(1,K+1),LDV,1,WORK,LDWORK);
                cublas_trmm('R','U',TRANST,'N',N,K,1,gT,LDT,WORK,LDWORK);
                if (M>K)
                    cublas_gemm('T','T',M-K,N,K,-1,V(1,K+1),LDV,WORK,LDWORK,1,C(K+1,1),LDC);
                cublas_trmm('R','U','N','U',N,K,1,V,LDV,WORK,LDWORK);

                for (J=1;J<=K;++J)
                {
                    for (I=1;I<=N;++I)
                    {
                        T tmp=DTH(C(I,N-K+J));
                        T tmp2=DTH(WORK(I,J));
                        HTD(C(I,N-K+J),tmp-tmp2);
                    }
                }
            }
        }
    }
    else if (bSTOREVR)
    {
        if (bDIRECT)
        {
            if (bSIDEL)
            {
                for (J=1;J<=K;++K)
                    cublas_copy(N,C(J,1),LDC,WORK(1,J),1);
                cublas_trmm('R','U','T','U',N,K,1,V,LDV,WORK,LDWORK);
                if (M>K)
                    cublas_gemm('T','T',N,K,M-K,1,C(K+1,1),LDC,V(1,K+1),LDV,1,WORK,LDWORK);
                cublas_trmm('R','U',TRANST,'N',N,K,1,gT,LDT,WORK,LDWORK);
                if (M>K)
                    cublas_gemm('T','T',M-K,N,K,-1,V(1,K+1),LDV,WORK,LDWORK,1,C(K+1,1),LDC);
                cublas_trmm('R','U','N','U',N,K,1,V,LDV,WORK,LDWORK);
                for (J=1;J<=K;++J)
                {
                    for (I=1;I<=N;++I)
                    {
                        T tmp=DTH(C(J,I));
                        T tmp2=DTH(WORK(I,J));
                        HTD(C(J,I),tmp-tmp2);
                    }
                }
            }
            else if (bSIDER)
            {
                for (J=1;J<=K;++J)
                    cublas_copy(M,C(1,J),1,WORK(1,J),1);
                cublas_trmm('R','U','T','U',M,K,1,V,LDV,WORK,LDWORK);
                if (N>K)
                    cublas_gemm('N','T',M,K,N-K,1,C(1,K+1),LDC,V(1,K+1),LDV,1,WORK,LDWORK);
                cublas_trmm('R','U',TRANS,'N',M,K,1,gT,LDT,WORK,LDWORK);
                if (N>K)
                    cublas_gemm('N','N',M,N-K,K,-1,WORK,LDWORK,V(1,K+1),LDV,1,C(1,K+1),LDC);
                cublas_trmm('R','U','N','U',M,K,1,V,LDV,WORK,LDWORK);
                for (J=1;J<=K;++J)
                {
                    for (I=1;I<=M;++I)
                    {
                        T tmp=DTH(C(I,J));
                        T tmp2=DTH(WORK(I,J));
                        HTD(C(I,J),tmp-tmp2);
                    }
                }
            }
        }
        else
        {
            if (bSIDEL)
            {
                for (J=1;J<=K;++J)
                    cublas_copy(N,C(M-K+J,1),LDC,WORK(1,J),1);
                cublas_trmm('R','L','T','U',N,K,1,V(1,M-K+1),LDV,WORK,LDWORK);
                if (M>K)
                    cublas_gemm('T','T',N,K,M-K,1,C,LDC,V,LDV,1,WORK,LDWORK);
                cublas_trmm('R','L',TRANST,'N',N,K,1,gT,LDT,WORK,LDWORK);
                if (M>K)
                    cublas_gemm('T','T',M-K,N,K,-1,V,LDV,WORK,LDWORK,1,C,LDC);
                cublas_trmm('R','L','N','U',N,K,1,V(1,M-K+1),LDV,WORK,LDWORK);
                for (J=1;J<=K;++J)
                {
                    for (I=1;I<=N;++I)
                    {
                        T tmp=DTH(C(M-K+J,I));
                        T tmp2=DTH(WORK(I,J));
                        HTD(C(M-K+J,I),tmp-tmp2);
                    }
                }
            }
            else if (bSIDER)
            {
                for (J=1;J<=K;++J)
                    cublas_copy(M,C(1,N-K+J),1,WORK(1,J),1);
                cublas_trmm('R','L','T','U',M,K,1,V(1,N-K+1),LDV,WORK,LDWORK);
                if (N>K)
                    cublas_gemm('N','T',M,K,N-K,1,C,LDC,V,LDV,1,WORK,LDWORK);
                cublas_trmm('R','L',TRANS,'N',M,K,1,gT,LDT,WORK,LDWORK);
                if (N>K)
                    cublas_gemm('N','N',M,N-K,K,-1,WORK,LDWORK,V,LDV,1,C,LDC);
                cublas_trmm('R','L','N','U',M,K,1,V(1,N-K+1),LDV,WORK,LDWORK);
                for (J=1;J<=K;++J)
                {
                    for (I=1;I<=M;++I)
                    {
                        T tmp=DTH(C(I,M-K+J));
                        T tmp2=DTH(WORK(I,J));
                        HTD(C(I,M-K+J),tmp-tmp2);
                    }
                }
            }
        }
    }
    return;
}
