/*
 * DGEHRD.cpp
 *
 *  Created on: 30 juin 2009
 *      Author: vlj
 */


#include <cstdlib>

#define TAU(i) TAU[(i)-1]
#define A(i,j) MIDX(A,LDA,i,j)
#define WORK(i) (WORK+(i)-1)

extern "C" int dlahr2_(int* N,int* K,int* NB, double *A,int* LDA,double* TAU,double* T,int* LDT,double* Y,int* LDY);

template<typename T>
void GEHRD(int N, int ILO, int IHI, T* A,int LDA, T* TAU, T* WORK, int LWORK, int &INFO)
{
	int I;
	int NBMAX=64;
	int LDT=NBMAX+1;
	int LDWORK=N;
	T* gT;
	T* T2=static_cast<T*>(malloc(LDT*NBMAX*sizeof(T)));
	T* cpuMat;
	T* WORK2=static_cast<T*>(malloc(LDWORK*BLOCK*sizeof(T)));
	T EI;



	cublasAlloc(LDT*NBMAX,sizeof(T),(void**)&gT);

	INFO=0;
	if(N<0)
		INFO=-1;
	if(ILO<1||ILO>MAX(1,N))
		INFO=-2;
	if(IHI<MIN(ILO,N)||IHI>N)
		INFO=-3;
	if(LDA<MAX(1,N))
		INFO=-5;
	if(INFO!=0)
		return;

	for(I=1;I<=ILO-1;++I)
	{
		TAU(I)=0;
	}
	for(I=MAX(1,IHI);I<=N-1;++I)
	{
		TAU(I)=0;
	}

	int NH=IHI-ILO+1;
	if(NH<=1)
	{
		WORK[0]=1;
		return;
	}

	int NB=BLOCK;
	I=1;

	cudaHostAlloc((void**)&cpuMat,LDA*N*sizeof(T),0);

	for(I=ILO;I<=IHI-1;I+=NB)
	{
		int IB=MIN(NB,IHI-I);
		modifiedLAHR2<T>(N,I,IB,cpuMat,A(1,I),LDA,&TAU(I),T2,gT,LDT,WORK2,WORK,LDWORK,EI);


		cublas_gemm('N','T',IHI,IHI-I-IB+1,IB,-1,WORK,LDWORK,A(I+IB,I),LDA,1,A(1,I+IB),LDA);
		HTD(A(I+IB,I+IB-1),EI);
		cublas_trmm('R','L','T','U',I,IB-1,1,A(I+1,I),LDA,WORK,LDWORK);
		for(unsigned int J=0;J<=IB-2;++J)
		{
			cublas_axpy(I,-1,WORK(LDWORK*J+1),1,A(1,I+J+1),1);
		}
		LARFB<T>('L','T','F','C',IHI-I,N-I-IB+1,IB,A(I+1,I),LDA,gT,LDT,A(I+1,I+IB),LDA,WORK,LDWORK);
	}
	//DGEHD2(N,I,IHI,A,LDA,TAU,WORK,IINFO);
}
