/*
 * DGEHD2.cpp
 *
 *  Created on: 30 juin 2009
 *      Author: vlj
 */



#define A(i,j) MIDX(A,LDA,i,j)
#define TAU(i) TAU[(i)-1]

void DGEHD2(int N, int ILO, int IHI, double* A, int LDA,double* TAU,double* WORK, int &INFO)
{
	int I;

	INFO=0;
	if(N<0)
		INFO=-1;
	if(ILO<1||IHI>MAX(1,N))
		INFO=-2;
	if(IHI<MIN(ILO,N)||IHI>N)
		INFO=-3;
	if(LDA<MAX(1,N))
		INFO=-5;
	if(INFO!=0)
		return;

	FFOR(I,ILO,IHI-1)
	{
		double dbl;
		DLARFG(IHI-I, A(I+1,I), A(MIN(I+2,N), I), 1, dbl);

		double AII=dblToHost(A(I+1,I),0);
		setSingleValue(A(I+1,I),0,1);
		DLARF('R', IHI, IHI-I, A(I+1,I), 1, dbl, A(1,I+1), LDA, WORK);
		DLARF('L', IHI-I, N-I, A(I+1,I), 1, dbl, A(I+1,I+1), LDA, WORK);
		setSingleValue(A(I+1,I),0,AII);
		TAU(I)=dbl;
	}

}
