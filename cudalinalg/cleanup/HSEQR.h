/*
 * DHSEQR.cpp
 *
 *  Created on: 30 juin 2009
 *      Author: vlj
 */

#include "cuLapack.h"

#define H(i,j) MIDX(H,LDH,i,j)

template<typename T>
void HSEQR(char JOB,char COMPZ,int N,int ILO,int IHI,
		T* H,int LDH,T* WR, T* WI,T* Z,int LDZ,T* WORK,int LWORK, int &INFO)
{
	bool WANTT=(JOB=='S'||JOB=='s');
	bool INITZ=(COMPZ=='I'||COMPZ=='i');
	bool WANTZ=INITZ||(COMPZ=='V'||COMPZ=='v');

	//TODO : make preliminary check
	LAHQR<T>(WANTT,WANTZ,N,ILO,IHI,H,LDH,WR,WI,ILO,IHI,Z,LDZ,INFO);

}
