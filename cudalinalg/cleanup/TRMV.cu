/*
 * customBlas.cpp
 *
 *  Created on: 17 juin 2009
 *      Author: vlj
 */

#include "cuLapack.h"


#include "kernels/transform.cu"

#define A(i,j) MIDX(A,LDA,i,j)
#define newA(i,j) MIDX(newA,LDA,i,j)

void DTRMV(char UPLO, char TRANSA, char DIAG,int N, double* A, int LDA, double* X, int INCX)
{
	int J;

	bool NOUNIT=(DIAG=='N'||DIAG=='n');
	bool UPPER=(UPLO=='U'||UPLO=='u');
	/*TODO : traditionnal check of input */

	double* newA;
	cublasAlloc(LDA*N,sizeof(double),(void**)&newA);
	cublasDcopy(LDA*N,A,1,newA,1);

	dim3 threads(BLOCKSIZE,BLOCKSIZE);
	dim3 grid(LDA/BLOCKSIZE+1,N/BLOCKSIZE+1);

	if(N==0)
		return;

	if(UPPER)
	{
		transform<'U'><<<grid,threads>>>(newA,LDA,N);
	}
	else
	{
		transform<'L'><<<grid,threads>>>(newA,LDA,N);
	}
	if(!NOUNIT)
	{
		FFOR(J,1,MIN(LDA,N))
		{
			HTD(newA(J,J),1);
		}
	}
	CHECK
	cublasDgemv(TRANSA,LDA,N,1,newA,LDA,X,INCX,0,X,INCX);
	CHECK
	cublasFree(newA);
	CHECK
}
