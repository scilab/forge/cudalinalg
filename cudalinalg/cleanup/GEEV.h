/*
 * DGEEV.cpp
 *
 *  Created on: 30 juin 2009
 *      Author: vlj
 */



#define VL(i,j) MIDX(VL,LDA,i,j)
#define A(i,j) MIDX(A,LDA,i,j)

template<typename T>
void GEEV(char JOBVL,char JOBVR,int N,T* A,int LDA,T* WR, T* WI,
		T* VL, int LDVL,T* VR, int LDVR, T* WORK, int LWORK, int &INFO)
{
	int I,J;
	int ILO=1;
	int IHI=N;

	INFO=0;
	bool WANTVL=(JOBVL=='V'||JOBVL=='v');
	bool WANTVR=(JOBVR=='V'||JOBVR=='v');
	bool NWANTVL=(JOBVL=='N'||JOBVL=='n');
	bool NWANTVR=(JOBVR=='N'||JOBVR=='n');

	if(!WANTVL && !NWANTVL)
		INFO=-1;
	if(!WANTVR && !NWANTVR)
		INFO=-2;
	if(N<0)
		INFO=-3;
	if(LDA<MAX(1,N))
		INFO=-5;
	if(LDVL<1 || (WANTVL && LDVL<N))
		INFO=-9;
	if(LDVR<1 || (WANTVR && LDVR<N))
		INFO=-11;
	if(INFO!=0)
		return;

	T* TAU=static_cast<T*>(malloc(N*sizeof(T)));
	//T TAU=T(N);
	int IERR;
	GEHRD<T>(N,ILO,IHI,A,LDA,TAU,WORK,LWORK,IERR);
	if(WANTVL)
	{
		cublas_copy(N*LDA,A,1,VL,1);
		for(I=1;I<=LDA;++I)
		{
			for(J=I+1;J<=N;++J)
			{
				HTD(VL(I,J),0);
			}
		}
		for(I=3;I<=LDA;++I)
		{
			for(J=1;J<=I-2;++J)
			{
				HTD(A(I,J),0);
			}
		}
		ORGHR<T>(N,ILO,IHI,VL,LDVL,TAU,WORK,LWORK,IERR);
		HSEQR<T>('S','V',N,ILO,IHI,A,LDA,WR,WI,VL,LDVL,WORK,LWORK,INFO);
	}
	//TODO : finish the function
}
