/*
 * DLANV2.cpp
 *
 *  Created on: 30 juin 2009
 *      Author: vlj
 */

#include "cuLapack.h"

template<typename T>
void LANV2(T &A, T &B, T &C, T &D,
		T &RT1R, T &RT1I, T &RT2R, T &RT2I, T &CS, T &SN)
{
	T TEMP,P,BCMAX,BCMIS,SCALE,Z,SIGMA,TAU,SAB,SAC,CS1,SN1;
	T AA,BB,CC,DD;

	if(C==0)
	{
		CS=1;
		SN=0;
		goto L10;
	}
	else if(B==0)
	{
		CS=0;
		SN=1;
		TEMP=D;
		D=A;
		A=TEMP;
		B=-C;
		C=0;
		goto L10;
	}
	else if((A-D)==0 && sign<T>(1,B)!=sign<T>(1,C))
	{
		CS=1;
		SN=0;
		goto L10;
	}
	else
	{
		TEMP=A-D;
		P=TEMP*0.5;
		BCMAX=MAX(std::abs(B),std::abs(C));
		BCMIS=MIN(std::abs(B),std::abs(C))*sign(1,B)*sign(1,C);
		SCALE=MAX(std::abs(P),BCMAX);
		Z=(P/SCALE)*P+(BCMAX/SCALE)*BCMIS;

		if(Z>=1E-5)
		{
			Z=P+sign<T>(sqrt(SCALE)*sqrt(Z),P);
			A=D+Z;
			D-=(BCMAX/Z)*BCMIS;
			TAU=sqrt(C*C+Z*Z);
			CS=Z/TAU;
			SN=C/TAU;
			B-=C;
			C=0;
		}
		else
		{
			SIGMA=B+C;
			TAU=sqrt(SIGMA*SIGMA+TEMP*TEMP);
			CS=sqrt(((std::abs(SIGMA))/TAU+1)*0.5);
			SN=-(P/(TAU*CS))*sign(1,SIGMA);
			AA=A*CS+B*SN;
			BB=-A*SN+B*CS;
			CC=C*CS+D*SN;
			DD=-C*SN+D*CS;

			A=AA*CS+CC*SN;
			B=BB*CS+DD*SN;
			C=-AA*SN+CC*CS;
			D=-BB*SN+DD*CS;

			TEMP=(A+D)*0.5;
			A=TEMP;
			D=TEMP;

			if(C!=0)
			{
				if(B!=0)
				{
					if(sign(1,B)==sign(1,C))
					{
						SAB=sqrt(std::abs(B));
						SAC=sqrt(std::abs(C));
						P=sign<T>(SAB*SAC,C);
						TAU=1/sqrt(std::abs(B+C));
						A=TEMP+P;
						D=TEMP-P;
						B-=C;
						C=0;
						CS1=SAB*TAU;
						SN1=SAC*TAU;
						TEMP=CS*CS1-SN*SN1;
						SN=CS*SN1+SN*CS1;
						CS=TEMP;
					}
				}
				else
				{
					B=-C;
					C=0;
					TEMP=CS;
					CS=-SN;
					SN=TEMP;
				}
			}
		}
	}


L10:

	RT1R=A;
	RT2R=D;
	if(C==0)
	{
		RT1I=0;
		RT2I=0;
	}
	else
	{
		RT1I=sqrt(std::abs(B))*sqrt(std::abs(C));
		RT2I=-RT1I;
	}
	return;

}
