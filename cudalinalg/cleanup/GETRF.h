/*!
 * \file DGETRF.cpp
 *
 *  Created on: 16 juil. 2009
 *  \Author: vlj
 */


#include "Exceptions.h"
#include "misc.h"
#include "LASWP.h"

#define A(i,j) MIDX(A,LDA,i,j)
#define TAU(i) TAU[(i)-1]
#define IPIV(i) IPIV[(i)-1]



template<typename T>
void GETRF(int M,int N,T* A,int LDA,int* IPIV,int &INFO)
{
	T* cpuBlock;

	/* PRELIMINARY CHECK OF INPUTS */
	INFO=0;
	if(M<0)
		throw argumentError("DGETRF",1);
	if(N<0)
		throw argumentError("DGETRF",2);
	if(LDA<MAX(1,M))
		throw argumentError("DGETRF",4);

	/* if A is an "empty" Matrix, don't do anything */
	if(M==0||N==0)
		return;
	/* Find block size for computation
	 * TODO : find a way to improve this parameter
	 */

	unsigned int NB=BLOCK;
	cudaHostAlloc((void**)&cpuBlock,LDA*NB*sizeof(T),0);

	/* if the block pattern is bigger than the matrix */
	if( NB>= MIN(M,N) )
	{
		int IINFO;

		cublasGetMatrix(M,N,sizeof(T),A,LDA,cpuBlock,LDA);
		_getrf(M,N,cpuBlock,LDA,IPIV,IINFO);
		cublasSetMatrix(M,N,sizeof(T),cpuBlock,LDA,A,LDA);
	}
	else
	{
		for(int J=1;J<=MIN(M,N);J+=NB)
		{
			int IINFO;
			/* Define the step size, NB if the reduced matrix is big enough, or the dimension of what is left otherwise. */
			int JB=MIN(MIN(M,N)-J+1,NB);
			int HEIGHT=M-J+1;

			cublasGetMatrix(HEIGHT,JB,sizeof(T),A(J,J),LDA,cpuBlock,LDA);
			_getrf(HEIGHT,JB,cpuBlock,LDA,&IPIV(J),IINFO);
			cublasSetMatrix(HEIGHT,JB,sizeof(T),cpuBlock,LDA,A(J,J),LDA);

			if(INFO==0 && IINFO>0)
			{
				INFO = IINFO + J - 1;
				throw algorithmFailure("DGETRF",INFO);
			}

			for(int I=J;I<=MIN(M,J+JB-1);++I)
			{
				IPIV(I) += J-1;
			}

			LASWP<T>(J-1,A,LDA,J,J+JB-1,IPIV,1);

			if(J+JB<=N)
			{
				LASWP<T>(N-J-JB+1,A(1,J+JB),LDA,J,J+JB-1,IPIV,1);
				cublas_trsm('L','L','N','U',JB,N-J-JB+1,1.0,A(J,J),LDA,A(J,J+JB),LDA);
				if(J+JB<=M)
				{
						cublas_gemm('N','N',M-J-JB+1,N-J-JB+1,JB,-1.0,A(J+JB,J),LDA,A(J,J+JB),LDA,1.0,A(J+JB,J+JB),LDA);

				}
			}

		}
	}

	cudaFreeHost(cpuBlock);
}
