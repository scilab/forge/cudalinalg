/*
 * DLAHQR.cpp
 *
 *  Created on: 30 juin 2009
 *      Author: vlj
 */

#include "cuLapack.h"

#define H(i,j) MIDX(H,LDH,i,j)
#define V(i) (V+(i)-1)

const double SMLNUM=1E-5;
const double ULP=1E-5;


void AED(int& L,double* H,int LDH,int ILO,int IHI,int I)
{

	unsigned int K;


/*
	 * Check if one of the subdiagonal element between ILO and IHI has become negligible
	 * during the previous QR iteration.
	 */
	for(K=I;K>=L+1;--K)
	{
		double value=DTH(H(K,K-1));
		double val=abs(value);
		if( val <= SMLNUM )
			break;
		double TST=abs(DTH(H(K-1,K-1)))+abs(DTH(H(K,K)));
		if(TST==0)
		{
			if(K-2>=ILO)
				TST+=abs(DTH(H(K-1,K-2)));
			if(K+1<=IHI)
				TST+=abs(DTH(H(K+1,K)));
		}
		if(abs(DTH(H(K,K-1)))<=ULP*TST)
		{
			double AB=MAX(abs(DTH(H(K,K-1))),abs(DTH(H(K-1,K))));
			double BA=MIN(abs(DTH(H(K,K-1))),abs(DTH(H(K-1,K))));
			double AA=MAX(abs(DTH(H(K,K))),abs(DTH(H(K-1,K-1))-DTH(H(K,K))));
			double BB=MIN(abs(DTH(H(K,K))),abs(DTH(H(K-1,K-1))-DTH(H(K,K))));
			double S=AA+AB;
			if(BA*(AB/S)<MAX(SMLNUM,ULP*(BB*(AA/S))))
				break;
		}
	}
	L=K;
	return;
}

bool measureImpact(double* H,int LDH,double RT1R,double RT1I,double RT2R,double RT2I,double* V,int M,int L)
{
	double HMM=DTH(H(M,M));
	double HMp1M=DTH(H(M+1,M));
	double HMMp1=DTH(H(M,M+1));
	double HMp1Mp1=DTH(H(M+1,M+1));
	double H21S=HMp1M;
	double S=abs(HMM-RT2R)+abs(RT2I)+abs(H21S);
	H21S=HMp1M/S;
	double V1=H21S*HMMp1+(HMM-RT1R)*((HMM-RT2R)/S)-RT1I*(RT2I/S);
	double V2=H21S*(HMM+HMp1Mp1-RT1R-RT2R);
	double V3=H21S*DTH(H(M+2,M+1));
	S=abs(V1)+abs(V2)+abs(V3);
	V1/=S;
	V2/=S;
	V3/=S;
	HTD(V(1),V1);
	HTD(V(2),V2);
	HTD(V(3),V3);
	if(M==L)
		return true;
	double tmp1=abs(DTH(H(M,M-1)))*(abs(DTH(V(2)))+abs(DTH(V(3))));
	double tmp2=ULP*abs(DTH(V(1)))*(
			abs( DTH(H(M-1,M-1)) )+abs( HMM )+abs( HMp1Mp1 )
			);
	if(tmp1<=tmp2)
		return true;

	return false;
}

void prepareShift(double H11,double H12,double H21,double H22,double &RT1R,double &RT1I,double &RT2R,double &RT2I)
{
	double S=abs(H11)+abs(H12)+abs(H21)+abs(H22);
	if(S==0)
	{
		RT1R=0;//Real part of eigenvalue#1 of the 2x2 submatrix.
		RT1I=0;//Imaginary part of eigenvalue#1 of the 2x2 submatrix.
		RT2R=0;//Real part of eigenvalue#2 of the 2x2 submatrix
		RT2I=0;//Imaginary part of eigenvalue#2 of the 2x2 submatrix.
	}
	else
	{
		H11/=S;
		H21/=S;
		H12/=S;
		H22/=S;
		double TR=(H11+H22)*0.5;
		double DET=(H11-TR)*(H22-TR)-H12*H21;
		double RTDISC=sqrt(abs(DET));
		if(DET>=0)
		{
			RT1R=TR*S;
			RT2R=RT1R;
			RT1I=RTDISC*S;
			RT2I=-RT1I;
		}
		else
		{
			RT1R=TR+RTDISC;
			RT2R=TR-RTDISC;
			if(abs(RT1R-H22)<=abs(RT2R-H22))
			{
				RT1R*=S;
				RT2R=RT1R;
			}
			else
			{
				RT2R*=S;
				RT1R=RT2R;
			}
			RT1I=0;
			RT2I=0;
		}
	}
	return;
}

void propagateQRFactorization(unsigned int NR, double *V,  unsigned  K, int I2, double *H, int LDH, double T1, double T2, double T3, int I1, int I)
{
	double cpuMat[9];
	double* tmpMat;
	cublasAlloc(9,sizeof(double),(void**)&tmpMat);
	double V2=DTH(V(2));
    if(NR==3)
		{
			double V3=DTH(V(3));

			cpuMat[0]=cpuMat[4]=cpuMat[8]=1;
			cpuMat[0]-=T1;
			cpuMat[3]-=V2*T1;
			cpuMat[6]-=V3*T1;
			cpuMat[1]-=T2;
			cpuMat[4]-=V2*T2;
			cpuMat[7]-=V3*T2;
			cpuMat[2]-=T3;
			cpuMat[5]-=T3*V2;
			cpuMat[8]-=T3*V3;

			cublasSetMatrix(3,3,sizeof(double),cpuMat,3,tmpMat,3);

			/*
			 * Apply the reflexion from K to I2 (columns).
			 */
			cublasDgemm('N','N',3,I2-K+1,3,1,tmpMat,3,H(K,K),LDH,0,H(K,K),LDH);


			/*
			 * Apply the reflexion from I1 to min(K+3,I) (columns).
			 */
			int length=MIN(K+3,I)-I1+1;
			cublasDgemm('N','T',length,3,3,1,H(I1,K),LDH,tmpMat,3,0,H(I1,K),LDH);

		}
				//TODO : accumulation in Z
		else if(NR==2)
		{
			/*
			 * Apply the reflexion from K to I2 (rows).
			 */
			for(unsigned int J=K;J<=I2;++J)
			{
				double hkj=DTH(H(K,J));
				double hkp1j=DTH(H(K+1,J));
				double SUM=hkj+V2*hkp1j;
				HTD(H(K,J),hkj-SUM*T1);
				HTD(H(K+1,J),hkp1j-SUM*T2);
			}

			/*
			 * Apply the reflexion from I1 to min(K+3,I) (rows).
			 */
			for(unsigned int J=I1;J<=I;++J)
			{
				double hjk=DTH(H(J,K));
				double hjkp1=DTH(H(J,K+1));
				double SUM=hjk+V2*hjkp1;
				HTD(H(J,K),hjk-SUM*T1);
				HTD(H(J,K+1),hjkp1-SUM*T2);
			}
		}

    cublasFree(tmpMat);
}

void bulgeChase(int I,int I1,int I2,int L,int M,double* H,int LDH,double* V)
{
	/*
	 * Double shifted QR step
	 * The first iteration will create a "bulge" on the left top hand matrix,
	 * further iteration will moved the bulge to the bottom of the matrix,
	 * eventually making it disappear.
	 */

	for(unsigned K=M;K<=I-1;++K)
	{
		unsigned int NR=MIN(3,I-K+1);
		if(K>M)//Iteration 2+
			cublasDcopy(NR,H(K,K-1),1,V(1),1);
		double T1;
		DLARFG(NR,V(1),V(2),1,T1);
		if(K>M)//Iteration 2+
		{
			double V1=DTH(V(1));
			HTD(H(K,K-1),V1);
			HTD(H(K+1,K-1),0);
			if(K<I-1)
				HTD(H(K+2,K-1),0);
		}
		else if(M>L)
		{
			double hkkm1=DTH(H(K,K-1));
			HTD(H(K,K-1),hkkm1*(1-T1));
		}
		double V2=DTH(V(2));
		double T2=T1*V2;
		double V3=DTH(V(3));
		double T3=T1*V3;
	    propagateQRFactorization(NR, V, K, I2, H, LDH, T1, T2,T3, I1, I);
	}

}


void subMatrixQRShift(int ITS, double * H, int LDH, int L, int I, double *V, int I1, int I2)
{
	double H11,H12,H21,H22;
	double RT1R,RT1I,RT2R,RT2I;
	double DAT1=0.75;
	double DAT2=-0.4375;
	int M;

    if(ITS==10)//Shift trick to speed-up the convergence.
	{
		double S=abs(DTH(H(L+1,L)))+abs(DTH(H(L+2,L+1)));
		H11=DAT1*S+DTH(H(L,L));
		H12=DAT2*S;
		H21=S;
		H22=H11;
	}
	else if(ITS==20)//Shift trick to speed-up the convergence.
	{
		double S=abs(DTH(H(I,I-1)))+abs(DTH(H(I-1,I-2)));
		H11=DAT1*S+DTH(H(I,I));
		H12=DAT2*S;
		H21=S;
		H22=H11;
	}
	else// "Francis double shift" as documented in DLAHQR.f on netlib
	{
		H11=DTH(H(I-1,I-1));
		H21=DTH(H(I,I-1));
		H12=DTH(H(I-1,I));
		H22=DTH(H(I,I));
	}
    prepareShift(H11, H12, H21, H22, RT1R, RT1I, RT2R, RT2I);

	for(M=I-2;M>=L;--M)
	{
		measureImpact(H,LDH,RT1R,RT1I,RT2R,RT2I,V,M,L);

	}
	M=L;

    bulgeChase(I, I1, I2, L, M, H, LDH, V);
}


template<typename T>
void LAHQR(bool WANTT,bool WANTZ,int N,int ILO,int IHI,T* H,int LDH,T* WR,T* WI,
		int ILOZ,int IHIZ,T* Z,int LDZ,int &INFO)
{
	double CS,SN;

	double* V;

#define WR(i) SIDX(WR,i)
#define WI(i) SIDX(WI,I)
	int L,ITS,ITMAX;
	int I1,I2,I,J;
	ITMAX=30;


	cublasAlloc(3,sizeof(double),(void**)&V);

	INFO=0;
	if(N==0)
		return;

	if(ILO==IHI)
	{
		WR(ILO)=DTH(H(ILO,ILO));
		WI(ILO)=0;
		return;
	}
	for(J=ILO;J<=IHI-3;++J)
	{
		HTD(H(J+2,J),0);
		HTD(H(J+3,J),0);
	}

	if(ILO<=IHI-2)
		HTD(H(IHI,IHI-2),0);


	if(WANTT)
	{
		I1=1;
		I2=N;
	}
	I=IHI;
L20:
	L=ILO;
	if(I<ILO)
		goto L160;

	for(ITS=0;ITS<=ITMAX;++ITS)
	{
		//AED(L,H,LDH,ILO,IHI,I);

		if(L>ILO)//H(L,L-1) is ~0.
			HTD(H(L,L-1),0);
		if(L>=I-1)
	        goto L150;

	    if(!WANTT){
	        I1 = L;
	        I2 = I;
	    }
	    //subMatrixQRShift(ITS, H, LDH, L, I, V, I1, I2);
	}


	INFO=I;
	return;

L150:
	if(L==I)
	{
		WR(I)=DTH(H(I,I));
		WI(I)=0;
	}
	else if(L==I-1)
	{
		double A=DTH(H(I-1,I-1));
		double B=DTH(H(I-1,I));
		double C=DTH(H(I,I-1));
		double D=DTH(H(I,I));
		double WRIm1=WR(I-1);
		double WIIm1=WI(I-1);
		double WRI=WR(I);
		double WII=WI(I);
		//DLANV2(A,B,C,D,WRIm1,WIIm1,WRI,WII,CS,SN);
		HTD(H(I-1,I-1),A);
		HTD(H(I-1,I),B);
		HTD(H(I,I-1),C);
		HTD(H(I,I),D);
		WR(I-1)=WRIm1;
		WR(I)=WRI;
		WI(I-1)=WIIm1;
		WI(I)=WII;
	}
	if(WANTT)
	{
		if(I2>I)
		{
			//cublasDrot(I2-I,H(I-1,I+1),LDH,H(I,I+1),LDH,CS,SN);
		}
	}
	I=L-1;
	goto L20;

L160:
	return;

}
